package hibernate.tricks.env;

import bitronix.tm.TransactionManagerServices;
import bitronix.tm.resource.jdbc.PoolingDataSource;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.transaction.Status;
import javax.transaction.UserTransaction;

public class TransactionManager {

    private static final String DATASOURCE_NAME = "myDataSource";

    private final Context context = new InitialContext();
    private final PoolingDataSource datasource;
    final DataSourceProducer databaseProduct;

    TransactionManager(DataSourceProducer dataSourceProducer,
                       String connectionURL) throws Exception {
        TransactionManagerServices.getConfiguration().setServerId("myServer1234");

        TransactionManagerServices.getConfiguration().setDisableJmx(true);

        TransactionManagerServices.getConfiguration().setJournal("null");

        TransactionManagerServices.getConfiguration().setWarnAboutZeroResourceTransaction(false);

        datasource = new PoolingDataSource();
        datasource.setUniqueName(DATASOURCE_NAME);
        datasource.setMinPoolSize(1);
        datasource.setMaxPoolSize(5);
        datasource.setPreparedStatementCacheSize(10);

        datasource.setIsolationLevel("READ_COMMITTED");

        datasource.setAllowLocalTransactions(true);

        this.databaseProduct = dataSourceProducer;
        dataSourceProducer.configuration.configure(datasource, connectionURL);

        datasource.init();
    }

    public UserTransaction getUserTransaction() {
        try {
            return (UserTransaction) this.context.lookup("java:comp/UserTransaction");
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    public void rollback() {
        UserTransaction tx = getUserTransaction();
        try {
            if (tx.getStatus() == Status.STATUS_ACTIVE ||
                tx.getStatus() == Status.STATUS_MARKED_ROLLBACK)
                tx.rollback();
        } catch (Exception ex) {
            ex.printStackTrace(System.err);
        }
    }

    public void stop() {
        datasource.close();
        TransactionManagerServices.getTransactionManager().shutdown();
    }

}
