package hibernate.tricks.env;

import bitronix.tm.resource.jdbc.PoolingDataSource;
import org.hibernate.dialect.H2Dialect;

public enum DataSourceProducer {

    H2(
            (ds, connectionURL) -> {
                ds.setClassName("org.h2.jdbcx.JdbcDataSource");

                ds.getDriverProperties().put("URL", connectionURL != null ? connectionURL : "jdbc:h2:mem:test");

                ds.getDriverProperties().put("user", "sa");
            },
        H2Dialect.class.getName()
    );

    public DataSourceConfiguration configuration;
    public String hibernateDialect;

    DataSourceProducer(DataSourceConfiguration configuration,
                       String hibernateDialect) {
        this.configuration = configuration;
        this.hibernateDialect = hibernateDialect;
    }

    public interface DataSourceConfiguration {

        void configure(PoolingDataSource ds, String connectionURL);
    }

}
