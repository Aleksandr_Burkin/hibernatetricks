package hibernate.tricks.env;

import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;

import java.util.Locale;

public class TransactionManagerTest {

    static public TransactionManager TM;

    @Parameters({"database", "connectionURL"})
    @BeforeSuite()
    public void beforeSuite(@Optional String database,
                            @Optional String connectionURL) throws Exception {
        TM = new TransactionManager(
            database != null
                ? DataSourceProducer.valueOf(database.toUpperCase(Locale.US))
                : DataSourceProducer.H2,
            connectionURL
        );
    }

    @AfterSuite(alwaysRun = true)
    public void afterSuite()  {
        if (TM != null) TM.stop();
    }
}
