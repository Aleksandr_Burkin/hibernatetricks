package hibernate.tricks.env;

import org.hibernate.internal.util.StringHelper;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.HashMap;
import java.util.Map;

public class JPASetup {

    private final String persistenceUnitName;
    private final Map<String, String> properties = new HashMap<>();
    private final EntityManagerFactory entityManagerFactory;

    JPASetup(DataSourceProducer dataSourceProducer,
             String persistenceUnitName,
             String... hbmResources) {

        this.persistenceUnitName = persistenceUnitName;

        properties.put("hibernate.archive.autodetection", "none");

        properties.put("hibernate.hbmxml.files", StringHelper.join(",", hbmResources != null ? hbmResources : new String[0]));

        properties.put("hibernate.format_sql", "true");

        properties.put("hibernate.use_sql_comments", "true");

        properties.put("hibernate.dialect", dataSourceProducer.hibernateDialect);

        entityManagerFactory = Persistence.createEntityManagerFactory(this.persistenceUnitName, properties);
    }

    EntityManagerFactory getEntityManagerFactory() {
        return entityManagerFactory;
    }

    public EntityManager createEntityManager() {
        return getEntityManagerFactory().createEntityManager();
    }

    void createSchema() {
        generateSchema("create");
    }

    void dropSchema() {
        generateSchema("drop");
    }

    private void generateSchema(String action) {
        Map<String, String> createSchemaProperties = new HashMap<>(properties);
        createSchemaProperties.put("javax.persistence.schema-generation.database.action", action);
        Persistence.generateSchema(this.persistenceUnitName, createSchemaProperties);
    }
}
