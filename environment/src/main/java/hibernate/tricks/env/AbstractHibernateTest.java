package hibernate.tricks.env;

import org.testng.annotations.*;

import java.util.Locale;

public abstract class AbstractHibernateTest {

    private String persistenceUnitName;
    private String[] hbmResources;
    protected JPASetup JPA;

    static public TransactionManager TM;

    @BeforeClass
    public void beforeClass() throws Exception {
        configurePersistenceUnit();
    }

    public void configurePersistenceUnit() throws Exception {
        configurePersistenceUnit(null);
    }

    public void configurePersistenceUnit(String persistenceUnitName, String... hbmResources) throws Exception {
        this.persistenceUnitName = persistenceUnitName;
        this.hbmResources = hbmResources;
    }

    @BeforeMethod
    public void beforeMethod() {
        JPA = new JPASetup(TM.databaseProduct, persistenceUnitName, hbmResources);

        JPA.dropSchema();

        JPA.createSchema();
    }

    @AfterMethod(alwaysRun = true)
    public void afterMethod() {
        if (JPA != null) {
            if (!"true".equals(System.getProperty("keepSchema")))
                JPA.dropSchema();

            JPA.getEntityManagerFactory().close();
        }
    }

    @Parameters({"database", "connectionURL"})
    @BeforeSuite()
    public void beforeSuite(@Optional String database,
                            @Optional String connectionURL) throws Exception {
        TM = new TransactionManager(
                database != null
                        ? DataSourceProducer.valueOf(database.toUpperCase(Locale.US))
                        : DataSourceProducer.H2,
                connectionURL
        );
    }

    @AfterSuite(alwaysRun = true)
    public void afterSuite()  {
        if (TM != null) TM.stop();
    }
}
