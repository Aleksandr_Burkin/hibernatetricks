package hibernate.tricks.model.mapping.collections.linkedorderby;

import javax.persistence.*;
import java.util.LinkedHashMap;
import java.util.Map;


/**
 * PRODUCT (ID)
 * GUIDE (PRODUCT_ID, FILENAME, AUTHOR)
 */
@Entity
public class Product {

    @Id
    @GeneratedValue
    private Long id;

    public Long getId() {
        return id;
    }

    @ElementCollection
    @CollectionTable(
            name = "GUIDE",
            joinColumns = @JoinColumn(name = "PRODUCT_ID")
    )
    @MapKeyColumn(name = "AUTHOR")
    @Column(name = "FILENAME")
    @org.hibernate.annotations.OrderBy(clause = "AUTHOR desc")
    private Map<String, String> guides = new LinkedHashMap<>();

    public Map<String, String> getGuides() {
        return guides;
    }
}
