package hibernate.tricks.model.mapping.collections.order;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


/**
 * PRODUCT (ID)
 * GUIDE (PRODUCT_ID, FILENAME, GUIDE_ORDER)
 *
 * Странное поведение в тестах.
 *
 */
@Entity
public class Product {

    @Id
    @GeneratedValue
    private Long id;

    public Long getId() {
        return id;
    }

    @ElementCollection
    @CollectionTable(
            name = "GUIDE",
            joinColumns = @JoinColumn(name = "PRODUCT_ID")
    )
    @OrderColumn(name = "GUIDE_ORDER")
    @Column(name = "FILENAME")
    private List<String> guides = new ArrayList<>();

    public Collection<String> getGuides() {
        return guides;
    }
}
