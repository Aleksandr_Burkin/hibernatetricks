package hibernate.tricks.model.mapping.associations.manytoone.jointable;

import javax.persistence.*;

@Entity
public class Guide {

    @Id
    @GeneratedValue
    private Long id;

    @Column(nullable = false)
    private String fileName;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinTable(
            name = "PRODUCT_GUIDE",
            joinColumns = @JoinColumn(name = "GUIDE_ID"),
            inverseJoinColumns = @JoinColumn(name = "PRODUCT_ID")
    )
    private Product product;

    public Guide() {
    }

    public Guide(String fileName) {
        this.fileName = fileName;
    }

    public Long getId() {
        return id;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }
}
