package hibernate.tricks.model.mapping.inheritance.tableperclass;

import javax.persistence.*;

@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public class ProductTablePerClass {

    @Id
    @GeneratedValue
    private Long id;

    //@NotNull doesn't work
    @Column(nullable = false)
    private String brand;

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }
}
