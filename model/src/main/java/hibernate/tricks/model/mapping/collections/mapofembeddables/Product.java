package hibernate.tricks.model.mapping.collections.mapofembeddables;

import javax.persistence.*;
import java.util.HashMap;
import java.util.Map;

@Entity
public class Product {

    @Id
    @GeneratedValue
    protected Long id;

    @ElementCollection
    @CollectionTable(name = "GUIDE")
    protected Map<Author, Guide> guides = new HashMap<Author, Guide>();

    public Long getId() {
        return id;
    }

    public Map<Author, Guide> getGuides() {
        return guides;
    }

    public void setGuides(Map<Author, Guide> images) {
        this.guides = images;
    }
}
