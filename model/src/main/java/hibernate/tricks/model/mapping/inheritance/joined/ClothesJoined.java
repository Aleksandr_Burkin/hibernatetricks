package hibernate.tricks.model.mapping.inheritance.joined;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
public class ClothesJoined extends ProductJoined {

    @Column(nullable = false)
    private String textile;

    @Column(nullable = false)
    private Integer size;

    public String getTextile() {
        return textile;
    }

    public void setTextile(String textile) {
        this.textile = textile;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }
}
