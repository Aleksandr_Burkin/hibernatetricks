package hibernate.tricks.model.mapping.inheritance.mappedsuperclass;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class CarMappedSuperclass extends ProductMappedSuperclass {

    @Id
    @GeneratedValue
    private Long id;

    @Column(nullable = false)
    private String engine;

    @Column(nullable = false)
    private Integer passengers;

    public String getEngine() {
        return engine;
    }

    public void setEngine(String engine) {
        this.engine = engine;
    }

    public Integer getPassengers() {
        return passengers;
    }

    public void setPassengers(Integer passengers) {
        this.passengers = passengers;
    }
}
