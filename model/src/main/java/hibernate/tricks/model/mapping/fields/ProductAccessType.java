package hibernate.tricks.model.mapping.fields;

import javax.persistence.*;

@Entity
public class ProductAccessType {

    private static final String ACCESS_FIELD  = "ACCESS_FIELD";
    private static final String ACCESS_PROPERTY = "ACCESS_PROPERTY";

    @Id
    @GeneratedValue
    private Long id;

    @Access(AccessType.FIELD)
    private String nameAccessField;

    @Access(AccessType.PROPERTY)
    private String nameAccessProperty;

    public String getNameAccessField() {
        return ACCESS_FIELD;
    }

    public void setNameAccessField(String nameAccessField) {
        this.nameAccessField = nameAccessField;
    }

    public String getNameAccessProperty() {
        return ACCESS_PROPERTY;
    }

    public void setNameAccessProperty(String nameAccessProperty) {
        this.nameAccessProperty = nameAccessProperty;
    }
}
