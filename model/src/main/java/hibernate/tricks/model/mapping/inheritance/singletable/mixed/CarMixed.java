package hibernate.tricks.model.mapping.inheritance.singletable.mixed;

import javax.persistence.*;

@Entity
@DiscriminatorValue("CAR")
@SecondaryTable(name = "CAR", pkJoinColumns = @PrimaryKeyJoinColumn(name = "CAR_ID"))
public class CarMixed extends ProductMixed {

    @Column(table = "CAR", nullable = false)
    private String engine;

    @Column(table = "CAR", nullable = false)
    private Integer passengers;

    public String getEngine() {
        return engine;
    }

    public void setEngine(String engine) {
        this.engine = engine;
    }

    public Integer getPassengers() {
        return passengers;
    }

    public void setPassengers(Integer passengers) {
        this.passengers = passengers;
    }
}
