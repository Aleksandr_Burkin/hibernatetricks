package hibernate.tricks.model.mapping.converter;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class AmountConverter implements AttributeConverter<Amount, String> {

    @Override
    public String convertToDatabaseColumn(Amount amount) {
        System.out.println(amount.toString());
        return amount.toString();
    }

    @Override
    public Amount convertToEntityAttribute(String s) {
        return Amount.fromString(s);
    }
}
