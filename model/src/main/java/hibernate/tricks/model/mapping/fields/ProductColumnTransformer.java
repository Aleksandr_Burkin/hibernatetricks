package hibernate.tricks.model.mapping.fields;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class ProductColumnTransformer {

    @Id
    @GeneratedValue
    private Long id;

    @org.hibernate.annotations.ColumnTransformer(
            read = "weight / 2.20462",
            write = "? * 2.20462"
    )
    private double weight;

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }
}
