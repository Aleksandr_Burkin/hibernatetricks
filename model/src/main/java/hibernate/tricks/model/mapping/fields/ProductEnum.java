package hibernate.tricks.model.mapping.fields;

import javax.persistence.*;

@Entity
public class ProductEnum {

    @Id
    @GeneratedValue
    private Long id;

    @Enumerated(EnumType.ORDINAL)
    private LevelType levelTypeOrdinal = LevelType.HIGH;

    @Enumerated(EnumType.STRING)
    private LevelType levelTypeString = LevelType.HIGH;

    public LevelType getLevelTypeOrdinal() {
        return levelTypeOrdinal;
    }

    public LevelType getLevelTypeString() {
        return levelTypeString;
    }
}
