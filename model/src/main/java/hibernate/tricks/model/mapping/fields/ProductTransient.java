package hibernate.tricks.model.mapping.fields;

import javax.persistence.*;

@Entity
public class ProductTransient {

    @Id
    @GeneratedValue
    private Long id;

    //@Transient
    private transient String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
