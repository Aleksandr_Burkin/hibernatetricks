package hibernate.tricks.model.mapping.associations.manytomany.simple;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Guide {

    @Id
    @GeneratedValue
    private Long id;

    @Column(nullable = false)
    private String title;

    @ManyToMany(mappedBy = "guides")
    private Set<Author> authors = new HashSet<>();

    public Guide() {
    }

    public Guide(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public Set<Author> getAuthors() {
        return authors;
    }
}
