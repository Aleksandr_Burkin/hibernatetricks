package hibernate.tricks.model.mapping.associations.manytoone.list;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;


/**
 * PRODUCT (ID)
 * GUIDE (ID, PRODUCT_ID, AUTHOR, FILENAME)
 *
 */
@Entity
public class Product {

    @Id
    @GeneratedValue
    private Long id;

    public Long getId() {
        return id;
    }

    @OneToMany(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "PRODUCT_ID", nullable = false)
    @OrderColumn(name = "GUIDE_POSITION", nullable = false)
    private List<Guide> guides = new ArrayList<>();

    public List<Guide> getGuides() {
        return guides;
    }
}
