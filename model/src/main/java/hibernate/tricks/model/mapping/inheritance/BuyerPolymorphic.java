package hibernate.tricks.model.mapping.inheritance;

import hibernate.tricks.model.mapping.inheritance.joined.ProductJoined;

import javax.persistence.*;

@Entity
public class BuyerPolymorphic {

    @Id
    @GeneratedValue
    private Long id;

    public Long getId() {
        return id;
    }

    @OneToOne(fetch = FetchType.LAZY)
    private ProductJoined product;

    public ProductJoined getProduct() {
        return product;
    }

    public void setProduct(ProductJoined product) {
        this.product = product;
    }
}
