package hibernate.tricks.model.mapping.fields;

import javax.persistence.*;
import java.util.Date;

@Entity
public class ProductTemporal {

    @Id
    @GeneratedValue
    private Long id;

    @Temporal(TemporalType.TIMESTAMP)
    @org.hibernate.annotations.CreationTimestamp
    private Date createdData;

    public Date getCreatedData() {
        return createdData;
    }

    public void setCreatedData(Date createdData) {
        this.createdData = createdData;
    }
}
