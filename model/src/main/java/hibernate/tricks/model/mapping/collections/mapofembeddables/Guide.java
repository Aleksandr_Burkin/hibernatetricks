package hibernate.tricks.model.mapping.collections.mapofembeddables;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.util.Objects;

@Embeddable
public class Guide {

    @Column(nullable = false)
    private String title;

    @Column(nullable = false)
    private Integer size;

    public Guide() {
    }

    public Guide(String title, Integer size) {
        this.title = title;
        this.size = size;
    }

    public String getTitle() {
        return title;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Guide guide = (Guide) o;
        return Objects.equals(title, guide.title) && Objects.equals(size, guide.size);
    }

    @Override
    public int hashCode() {
        return Objects.hash(title, size);
    }
}
