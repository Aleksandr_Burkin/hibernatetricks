package hibernate.tricks.model.mapping.fields;

public enum LevelType {

    HIGH,
    LOW;
}
