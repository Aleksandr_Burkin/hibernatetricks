package hibernate.tricks.model.mapping.associations.onetoone.generalpk;

import javax.persistence.*;


/**
 * PRODUCT (ID)
 * GUIDE (ID, PRODUCT_ID, FILENAME)
 *
 */
@Entity
public class Product {

    @Id
    @GeneratedValue
    private Long id;

    public Long getId() {
        return id;
    }

}
