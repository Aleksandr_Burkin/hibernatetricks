package hibernate.tricks.model.mapping.fields;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Lob;
import java.sql.Blob;

@Entity
public class ProductBlob {

    @Id
    @GeneratedValue
    private Long id;

    public Long getId() {
        return id;
    }

    @Lob
    private Blob blob;

    public Blob getBlob() {
        return blob;
    }

    public void setBlob(Blob blob) {
        this.blob = blob;
    }
}
