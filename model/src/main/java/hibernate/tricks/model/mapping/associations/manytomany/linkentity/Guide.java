package hibernate.tricks.model.mapping.associations.manytomany.linkentity;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Guide {

    @Id
    @GeneratedValue
    private Long id;

    @Column(nullable = false)
    private String title;

    @OneToMany(mappedBy = "guide")
    private Set<AuthorGuide> authorGuides = new HashSet<>();

    public Guide() {
    }

    public Guide(String title) {
        this.title = title;
    }

    public Long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public Set<AuthorGuide> getAuthorGuides() {
        return authorGuides;
    }
}
