package hibernate.tricks.model.mapping.collections.order;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class GuideId implements Serializable {

    @Column(name = "PRODUCT_ID")
    private Long productId;

    @Column(name = "FILENAME")
    private String fileName;

    public GuideId(Long productId, String fileName) {
        this.productId = productId;
        this.fileName = fileName;
    }

    public String getFileName() {
        return fileName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GuideId guideId = (GuideId) o;
        return Objects.equals(productId, guideId.productId) &&
                Objects.equals(fileName, guideId.fileName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(productId, fileName);
    }
}
