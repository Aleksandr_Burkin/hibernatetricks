package hibernate.tricks.model.mapping.inheritance.joined;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.validation.constraints.NotNull;

@Entity
public class CarJoined extends ProductJoined {

    @Column(nullable = false)
    private String engine;

    @NotNull
    private Integer passengers;

    public String getEngine() {
        return engine;
    }

    public void setEngine(String engine) {
        this.engine = engine;
    }

    public Integer getPassengers() {
        return passengers;
    }

    public void setPassengers(Integer passengers) {
        this.passengers = passengers;
    }
}
