package hibernate.tricks.model.mapping.collections.map;

public enum Language {

    RUSSIAN,
    ENGLISH,
    DEUTSCHE
}
