package hibernate.tricks.model.mapping.collections.setofembeddables;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.util.Objects;

@Embeddable
public class Guide {

    @org.hibernate.annotations.Parent
    private Product parent;

    public Guide() {
    }

    public Guide(String author, String fileName) {
        this.author = author;
        this.fileName = fileName;
    }

    @Column(nullable = false)
    private String author;

    @Column(nullable = false)
    private String fileName;

    public Product getParent() {
        return parent;
    }

    public void setParent(Product parent) {
        this.parent = parent;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Guide guide = (Guide) o;
        return Objects.equals(parent.getId(), guide.parent.getId()) &&
                Objects.equals(fileName, guide.fileName) &&
                Objects.equals(author, guide.author);
    }

    @Override
    public int hashCode() {
        return Objects.hash(fileName, author);
    }
}
