package hibernate.tricks.model.mapping.collections.order;

import javax.persistence.*;

@Entity
public class Guide {

    @EmbeddedId
    private GuideId id;

    @Column(name = "GUIDE_ORDER")
    private Long guideOrder;


    public void setId(GuideId id) {
        this.id = id;
    }

    public Long getGuideOrder() {
        return guideOrder;
    }

    public void setGuideOrder(Long guideOrder) {
        this.guideOrder = guideOrder;
    }

    public String getFilename() {
        return id.getFileName();
    }
}
