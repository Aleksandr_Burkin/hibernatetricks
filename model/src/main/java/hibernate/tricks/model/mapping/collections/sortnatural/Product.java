package hibernate.tricks.model.mapping.collections.sortnatural;

import javax.persistence.*;
import java.util.SortedMap;
import java.util.TreeMap;


/**
 * PRODUCT (ID)
 * GUIDE (PRODUCT_ID, FILENAME, AUTHOR)
 *
 */
@Entity
public class Product {

    @Id
    @GeneratedValue
    private Long id;

    public Long getId() {
        return id;
    }

    @ElementCollection
    @CollectionTable(
            name = "GUIDE",
            joinColumns = @JoinColumn(name = "PRODUCT_ID")
    )
    @MapKeyColumn(name = "AUTHOR")
    @Column(name = "FILENAME")
    @org.hibernate.annotations.SortNatural
    private SortedMap<String, String> guides = new TreeMap<>();

    public SortedMap<String, String> getGuides() {
        return guides;
    }
}
