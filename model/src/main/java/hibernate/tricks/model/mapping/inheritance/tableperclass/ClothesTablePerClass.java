package hibernate.tricks.model.mapping.inheritance.tableperclass;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
public class ClothesTablePerClass extends ProductTablePerClass {

    @Column(nullable = false)
    private String textile;

    @Column(nullable = false)
    private Integer size;

    public String getTextile() {
        return textile;
    }

    public void setTextile(String textile) {
        this.textile = textile;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }
}
