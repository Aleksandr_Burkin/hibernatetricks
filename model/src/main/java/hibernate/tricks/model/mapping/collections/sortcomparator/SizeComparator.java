package hibernate.tricks.model.mapping.collections.sortcomparator;

import java.util.Comparator;

public class SizeComparator implements Comparator<Integer> {

    @Override
    public int compare(Integer o1, Integer o2) {
        return o1 - o2;
    }
}
