package hibernate.tricks.model.mapping.associations.onetoone.autogeneralpk;

import javax.persistence.*;


/**
 * PRODUCT (ID)
 * GUIDE (ID, PRODUCT_ID, FILENAME)
 *
 */
@Entity
public class Product {

    @Id
    @GeneratedValue
    private Long id;

    public Long getId() {
        return id;
    }

    @OneToOne(mappedBy = "product", cascade = CascadeType.PERSIST)
    private Guide guide;

    public Guide getGuide() {
        return guide;
    }

    public void setGuide(Guide guide) {
        this.guide = guide;
    }
}
