package hibernate.tricks.model.mapping.collections.map;

import javax.persistence.*;
import java.util.HashMap;
import java.util.Map;


/**
 * PRODUCT (ID)
 * GUIDE (PRODUCT_ID, FILENAME, LANGUAGE)
 *
 */
@Entity
public class Product {

    @Id
    @GeneratedValue
    private Long id;

    public Long getId() {
        return id;
    }

    @ElementCollection
    @CollectionTable(
            name = "GUIDE",
            joinColumns = @JoinColumn(name = "PRODUCT_ID")
    )
    @MapKeyEnumerated
    @MapKeyColumn(name = "LANGUAGE")
    @Column(name = "FILENAME")
    private Map<Language, String> guides = new HashMap<>();

    public Map<Language, String> getGuides() {
        return guides;
    }
}
