package hibernate.tricks.model.mapping.inheritance.mappedsuperclass;

import javax.persistence.MappedSuperclass;

@MappedSuperclass
public class ProductMappedSuperclass {

    private String brand;

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }
}
