package hibernate.tricks.model.mapping.associations.manytoone.set;

import javax.persistence.*;
import java.util.Objects;

@Entity
public class Guide {

    @Id
    @GeneratedValue
    private Long id;

    @Column(nullable = false)
    private String author;

    @Column(nullable = false)
    private String fileName;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PRODUCT_ID", nullable = false)
    private Product product;

    public Guide() {
    }

    public Guide(String author, String fileName, Product product) {
        this.author = author;
        this.fileName = fileName;
        this.product = product;
    }

    public Product getProduct() {
        return product;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Guide guide = (Guide) o;
        return Objects.equals(author, guide.author) &&
                Objects.equals(fileName, guide.fileName) &&
                Objects.equals(product, guide.product);
    }

    @Override
    public int hashCode() {
        return Objects.hash(author, fileName, product);
    }
}
