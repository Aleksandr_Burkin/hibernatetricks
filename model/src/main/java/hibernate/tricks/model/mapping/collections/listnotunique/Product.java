package hibernate.tricks.model.mapping.collections.listnotunique;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;


/**
 * PRODUCT (ID)
 * GUIDE (GUIDE_ID, PRODUCT_ID, FILENAME)
 */
@Entity
public class Product {

    @Id
    @GeneratedValue
    private Long id;

    public Long getId() {
        return id;
    }

    @ElementCollection
    @CollectionTable(
            name = "GUIDE",
            joinColumns = @JoinColumn(name = "PRODUCT_ID") //необязательный атрибут (генерируется автоматически)
    )
    @org.hibernate.annotations.CollectionId(
            columns = @Column(name = "GUIDE_ID"),
            type = @org.hibernate.annotations.Type(type = "long"),
            generator = "ID_GENERATOR"
    )
    @Column(name = "FILENAME")
    private Collection<String> guides = new ArrayList<>();

    public Collection<String> getGuides() {
        return guides;
    }
}
