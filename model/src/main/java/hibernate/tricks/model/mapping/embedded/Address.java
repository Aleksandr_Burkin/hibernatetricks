package hibernate.tricks.model.mapping.embedded;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class Address {

    private String country;

    @AttributeOverrides({
            @AttributeOverride(name = "name", column = @Column(name = "CITY")),
    })
    private City city;

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }
}
