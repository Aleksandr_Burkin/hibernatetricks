package hibernate.tricks.model.mapping.associations.onetoone.foreignkey;

import javax.persistence.*;

@Entity
public class Guide {

    @Id
    @GeneratedValue
    private Long id;

    @Column(nullable = false)
    private String fileName;

    public Guide() {
    }

    public Guide(String fileName) {
        this.fileName = fileName;
    }

    public Long getId() {
        return id;
    }
}
