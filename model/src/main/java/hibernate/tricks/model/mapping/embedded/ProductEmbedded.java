package hibernate.tricks.model.mapping.embedded;

import javax.persistence.*;

@Entity
public class ProductEmbedded {

    @Id
    @GeneratedValue
    private Long id;

    @Embedded //если есть @Embeddable, то необязательна и наоборот
    private Address storage;

    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "country", column = @Column(name = "DELIVERY_COUNTRY")),
            @AttributeOverride(name = "city.name", column = @Column(name = "DELIVERY_CITY")),
            @AttributeOverride(name = "city.street", column = @Column(name = "DELIVERY_STREET"))
    })
    private Address delivery;

    public Address getStorage() {
        return storage;
    }

    public void setStorage(Address storage) {
        this.storage = storage;
    }

    public Address getDelivery() {
        return delivery;
    }

    public void setDelivery(Address delivery) {
        this.delivery = delivery;
    }
}
