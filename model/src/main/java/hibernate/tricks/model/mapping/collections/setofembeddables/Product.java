package hibernate.tricks.model.mapping.collections.setofembeddables;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;


/**
 * PRODUCT (ID)
 * GUIDE (PRODUCT_ID, FILENAME)
 *
 */
@Entity
public class Product {

    @Id
    @GeneratedValue
    private Long id;

    public Long getId() {
        return id;
    }

    @ElementCollection
    @CollectionTable(
            name = "GUIDE",
            joinColumns = @JoinColumn(name = "PRODUCT_ID")
    )
    private Set<Guide> guides = new HashSet<>();

    public Set<Guide> getGuides() {
        return guides;
    }
}
