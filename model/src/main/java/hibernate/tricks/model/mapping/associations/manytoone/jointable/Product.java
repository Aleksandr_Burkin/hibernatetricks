package hibernate.tricks.model.mapping.associations.manytoone.jointable;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


/**
 * PRODUCT (ID)
 * GUIDE (ID, PRODUCT_ID, AUTHOR, FILENAME)
 *
 */
@Entity
public class Product {

    @Id
    @GeneratedValue
    private Long id;

    public Long getId() {
        return id;
    }

    @OneToMany(mappedBy = "product", cascade = CascadeType.PERSIST)
    private Set<Guide> guides = new HashSet<>();

    public Set<Guide> getGuides() {
        return guides;
    }
}
