package hibernate.tricks.model.mapping.inheritance.joined;

import javax.persistence.*;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public class ProductJoined {

    @Id
    @GeneratedValue
    private Long id;

    public Long getId() {
        return id;
    }

    //@NotNull doesn't work
    @Column(nullable = false)
    private String brand;

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }
}
