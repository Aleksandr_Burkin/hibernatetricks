package hibernate.tricks.model.mapping.collections.sortcomparator;

import javax.persistence.*;
import java.util.SortedMap;
import java.util.TreeMap;


/**
 * PRODUCT (ID)
 * GUIDE (PRODUCT_ID, FILENAME, SIZE)
 *
 */
@Entity
public class Product {

    @Id
    @GeneratedValue
    private Long id;

    public Long getId() {
        return id;
    }

    @ElementCollection
    @CollectionTable(
            name = "GUIDE",
            joinColumns = @JoinColumn(name = "PRODUCT_ID")
    )
    @MapKeyColumn(name = "SIZE")
    @Column(name = "FILENAME")
    @org.hibernate.annotations.SortComparator(SizeComparator.class)
    private SortedMap<Integer, String> guides = new TreeMap<>();

    public SortedMap<Integer, String> getGuides() {
        return guides;
    }
}
