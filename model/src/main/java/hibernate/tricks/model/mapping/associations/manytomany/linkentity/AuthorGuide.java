package hibernate.tricks.model.mapping.associations.manytomany.linkentity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name = "AUTHOR_GUIDE")
@org.hibernate.annotations.Immutable
public class AuthorGuide {

    @Embeddable
    public static class Id implements Serializable {

        @Column(name = "AUTHOR_ID")
        private Long authorId;

        @Column(name = "GUIDE_ID")
        private Long guideId;

        public Id() {
        }

        public Id(Long authorId, Long guideId) {
            this.authorId = authorId;
            this.guideId = guideId;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Id id = (Id) o;
            return Objects.equals(authorId, id.authorId) &&
                    Objects.equals(guideId, id.guideId);
        }

        @Override
        public int hashCode() {
            return Objects.hash(authorId, guideId);
        }
    }

    @EmbeddedId
    private Id id = new Id();

    @Column(name = "ADDED_BY", updatable = false)
    private String addedBy;

    @Temporal(value = TemporalType.TIMESTAMP)
    @Column(name = "CREATED_ON", updatable = false)
    private Date createdOn = new Date();

    @ManyToOne
    @JoinColumn(name = "AUTHOR_ID", insertable = false, updatable = false)
    private Author author;

    @ManyToOne
    @JoinColumn(name = "GUIDE_ID", insertable = false, updatable = false)
    private Guide guide;

    public AuthorGuide() {
    }

    public AuthorGuide(String addedBy, Author author, Guide guide) {
        this.addedBy = addedBy;

        this.author = author;
        this.guide = guide;

        this.id.authorId = author.getId();
        this.id.guideId = guide.getId();

        this.author.getAuthorGuides().add(this);
        this.guide.getAuthorGuides().add(this);
    }

    public Id getId() {
        return id;
    }

    public String getAddedBy() {
        return addedBy;
    }

    public Author getAuthor() {
        return author;
    }

    public Guide getGuide() {
        return guide;
    }
}
