package hibernate.tricks.model.mapping.converter;

import javax.persistence.*;

@Entity
public class ProductConverter {

    @Id
    @GeneratedValue
    private Long id;

    @Convert(converter = AmountConverter.class)
    private Amount price;

    public Amount getPrice() {
        return price;
    }

    public void setPrice(Amount price) {
        this.price = price;
    }
}
