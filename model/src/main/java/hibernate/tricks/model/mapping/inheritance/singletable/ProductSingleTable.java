package hibernate.tricks.model.mapping.inheritance.singletable;

import javax.persistence.*;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "PRODUCT_TYPE")
public class ProductSingleTable {

    @Id
    @GeneratedValue
    private Long id;

    //@NotNull doesn't work
    @Column(nullable = false)
    private String brand;

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }
}
