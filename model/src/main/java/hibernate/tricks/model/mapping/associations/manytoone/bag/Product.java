package hibernate.tricks.model.mapping.associations.manytoone.bag;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;


/**
 * PRODUCT (ID)
 * GUIDE (ID, PRODUCT_ID, AUTHOR, FILENAME)
 *
 */
@Entity
public class Product {

    @Id
    @GeneratedValue
    private Long id;

    public Long getId() {
        return id;
    }

    @OneToMany(mappedBy = "product", cascade = CascadeType.PERSIST)
    private Collection<Guide> guides = new ArrayList<>();

    public Collection<Guide> getGuides() {
        return guides;
    }
}
