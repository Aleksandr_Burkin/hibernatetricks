package hibernate.tricks.model.mapping.associations.onetoone.autogeneralpk;

import javax.persistence.*;

@Entity
public class Guide {

    @Id
    @GeneratedValue(generator = "guideKeyGenerator")
    @org.hibernate.annotations.GenericGenerator(
            name = "guideKeyGenerator",
            strategy = "foreign",
            parameters = @org.hibernate.annotations.Parameter(
                    name = "property", value = "product"
            )
    )
    private Long id;

    @Column(nullable = false)
    private String fileName;

    @OneToOne(optional = false)
    @PrimaryKeyJoinColumn
    private Product product;

    public Guide() {
    }

    public Guide(Product product, String fileName) {
        this.product = product;
        this.fileName = fileName;
    }

    public Product getProduct() {
        return product;
    }
}
