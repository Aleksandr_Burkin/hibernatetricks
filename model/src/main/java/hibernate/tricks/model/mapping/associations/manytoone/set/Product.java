package hibernate.tricks.model.mapping.associations.manytoone.set;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;


/**
 * PRODUCT (ID)
 * GUIDE (ID, PRODUCT_ID, AUTHOR, FILENAME)
 *
 */
@Entity
public class Product {

    @Id
    @GeneratedValue
    private Long id;

    public Long getId() {
        return id;
    }

    @OneToMany(mappedBy = "product", fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    private Set<Guide> guides = new HashSet<>();

    public Set<Guide> getGuides() {
        return guides;
    }
}
