package hibernate.tricks.model.mapping.associations.manytoone.cascaderemove;

import javax.persistence.*;

@Entity
public class Guide {

    @Id
    @GeneratedValue
    private Long id;

    @Column(nullable = false)
    private String author;

    @Column(nullable = false)
    private String fileName;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PRODUCT_ID", nullable = false)
    private Product product;

    public Guide() {
    }

    public Guide(String author, String fileName, Product product) {
        this.author = author;
        this.fileName = fileName;
        this.product = product;
    }

    public Product getProduct() {
        return product;
    }
}
