package hibernate.tricks.model.mapping.associations.onetoone.generalpk;

import javax.persistence.*;

@Entity
public class Guide {

    @Id
    private Long id;

    @Column(nullable = false)
    private String fileName;

    @OneToOne(fetch = FetchType.LAZY, optional = false)
    @PrimaryKeyJoinColumn
    private Product product;

    public Guide() {
    }

    public Guide(Long id, String fileName) {
        this.id = id;
        this.fileName = fileName;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }
}
