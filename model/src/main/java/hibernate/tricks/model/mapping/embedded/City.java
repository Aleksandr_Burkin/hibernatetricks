package hibernate.tricks.model.mapping.embedded;

import javax.persistence.Embeddable;

@Embeddable
public class City {

    private String name;

    private String street;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }
}
