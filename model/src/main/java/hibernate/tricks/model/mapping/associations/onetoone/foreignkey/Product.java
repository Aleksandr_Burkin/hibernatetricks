package hibernate.tricks.model.mapping.associations.onetoone.foreignkey;

import javax.persistence.*;


/**
 * PRODUCT (ID, GUIDE_ID)
 * GUIDE (ID, FILENAME)
 *
 */
@Entity
public class Product {

    @Id
    @GeneratedValue
    private Long id;

    public Long getId() {
        return id;
    }

    @OneToOne(fetch = FetchType.LAZY, optional = false, cascade = CascadeType.PERSIST)
    @JoinColumn(unique = true)
    private Guide guide;

    public Guide getGuide() {
        return guide;
    }

    public void setGuide(Guide guide) {
        this.guide = guide;
    }
}
