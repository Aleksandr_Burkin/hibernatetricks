package hibernate.tricks.model.mapping.associations.onetoone.jointable;

import javax.persistence.*;


/**
 * PRODUCT (ID)
 * PRODUCT_GUIDE (PRODUCT_ID, GUIDE_ID)
 * GUIDE (ID, FILENAME)
 *
 * Использовать когда связь oneToOne необязательна т.е. когда guide может быть null
 *
 */
@Entity
public class Product {

    @Id
    @GeneratedValue
    private Long id;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinTable(
            name = "PRODUCT_GUIDE",
            joinColumns = @JoinColumn(name = "PRODUCT_ID"),
            inverseJoinColumns = @JoinColumn(name = "GUIDE_ID", nullable = true, unique = true)
    )
    private Guide guide;

    public Product() {

    }

    public Product(Guide guide) {
        this.guide = guide;
    }

    public Long getId() {
        return id;
    }

    public Guide getGuide() {
        return guide;
    }
}
