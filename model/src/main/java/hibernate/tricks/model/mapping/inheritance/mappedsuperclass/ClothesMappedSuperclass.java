package hibernate.tricks.model.mapping.inheritance.mappedsuperclass;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class ClothesMappedSuperclass extends ProductMappedSuperclass {

    @Id
    @GeneratedValue
    private Long id;

    @Column(nullable = false)
    private String textile;

    @Column(nullable = false)
    private Integer size;

    public String getTextile() {
        return textile;
    }

    public void setTextile(String textile) {
        this.textile = textile;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }
}
