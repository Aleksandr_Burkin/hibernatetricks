package hibernate.tricks.mapping.associations.onetoone;

import hibernate.tricks.env.AbstractHibernateTest;
import hibernate.tricks.model.mapping.associations.onetoone.autogeneralpk.Guide;
import hibernate.tricks.model.mapping.associations.onetoone.autogeneralpk.Product;
import org.testng.annotations.Test;

import javax.persistence.EntityManager;
import javax.transaction.UserTransaction;

import static org.testng.AssertJUnit.assertSame;

public class OneToOneAutoGeneralPkTest extends AbstractHibernateTest {

    private static final String FILE_NAME_1 = "FILE_NAME_1";

    @Override
    public void configurePersistenceUnit() throws Exception {
        configurePersistenceUnit("OneToOneAutoGeneralPkPU");
    }

    @Test
    public void testOneToOneAutoGeneral() {
        UserTransaction tx = TM.getUserTransaction();
        long PRODUCT_ID;

        try {
            {
                tx.begin();

                EntityManager em = JPA.createEntityManager();

                Product product = new Product();

                Guide guide = new Guide(product, FILE_NAME_1);

                product.setGuide(guide);

                em.persist(guide);
                PRODUCT_ID = product.getId();

                tx.commit();
                em.close();
            }
            {
                tx.begin();

                EntityManager em = JPA.createEntityManager();

                Product product = em.find(Product.class, PRODUCT_ID);
                Guide guide = em.find(Guide.class, PRODUCT_ID);
                assertSame(product, guide.getProduct());

                tx.commit();
                em.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            TM.rollback();
        }
    }
}
