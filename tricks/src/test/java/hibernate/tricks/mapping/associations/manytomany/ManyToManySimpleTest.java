package hibernate.tricks.mapping.associations.manytomany;

import hibernate.tricks.env.AbstractHibernateTest;
import hibernate.tricks.model.mapping.associations.manytomany.simple.Author;
import hibernate.tricks.model.mapping.associations.manytomany.simple.Guide;
import org.testng.annotations.Test;

import javax.persistence.EntityManager;
import javax.transaction.UserTransaction;
import java.util.List;

import static org.testng.AssertJUnit.assertEquals;

public class ManyToManySimpleTest extends AbstractHibernateTest {

    private static final String AUTHOR_NAME_1 = "AUTHOR_NAME_1";
    private static final String AUTHOR_NAME_2 = "AUTHOR_NAME_2";

    private static final String TITLE_1 = "TITLE_1";
    private static final String TITLE_2 = "TITLE_2";

    private static final String SQL = "SELECT * FROM author_guide";

    @Override
    public void configurePersistenceUnit() throws Exception {
        configurePersistenceUnit("ManyToManySimplePU");
    }

    @Test
    public void testManyToManySimple() {
        UserTransaction tx = TM.getUserTransaction();
        long AUTHOR_ID_1;
        long AUTHOR_ID_2;

        try {
            {
                tx.begin();

                EntityManager em = JPA.createEntityManager();

                Author author_1 = new Author(AUTHOR_NAME_1);
                Author author_2 = new Author(AUTHOR_NAME_2);

                Guide guide_1 = new Guide(TITLE_1);
                Guide guide_2 = new Guide(TITLE_2);

                author_1.getGuides().add(guide_1);
                author_1.getGuides().add(guide_2);

                guide_1.getAuthors().add(author_1);
                guide_1.getAuthors().add(author_2);

                author_2.getGuides().add(guide_1);

                guide_2.getAuthors().add(author_1);

                em.persist(author_1);
                em.persist(author_2);

                AUTHOR_ID_1 = author_1.getId();
                AUTHOR_ID_2 = author_2.getId();

                tx.commit();
                em.close();
            }
            {
                tx.begin();

                EntityManager em = JPA.createEntityManager();

                Author author_1 = em.find(Author.class, AUTHOR_ID_1);
                assertEquals(2, author_1.getGuides().size());

                Author author_2 = em.find(Author.class, AUTHOR_ID_2);
                assertEquals(1, author_2.getGuides().size());

                List resultList = em.createNativeQuery(SQL).getResultList();
                assertEquals(3, resultList.size());

                tx.commit();
                em.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            TM.rollback();
        }
    }
}
