package hibernate.tricks.mapping.associations.manytoone;

import hibernate.tricks.env.AbstractHibernateTest;
import hibernate.tricks.model.mapping.associations.manytoone.jointable.Guide;
import hibernate.tricks.model.mapping.associations.manytoone.jointable.Product;
import org.testng.annotations.Test;

import javax.persistence.EntityManager;
import javax.transaction.UserTransaction;

import static org.testng.AssertJUnit.*;

public class ManyToManyJoinTableTest extends AbstractHibernateTest {

    private static final String FILE_NAME_1 = "FILE_NAME_1";

    @Override
    public void configurePersistenceUnit() throws Exception {
        configurePersistenceUnit("ManyToOneJoinTablePU");
    }

    @Test
    public void testManyToOneJoinTable() {
        UserTransaction tx = TM.getUserTransaction();
        long PRODUCT_ID_1;
        long PRODUCT_ID_2;
        long GUIDE_ID;

        try {
            {
                tx.begin();

                EntityManager em = JPA.createEntityManager();

                Product product_1 = new Product();
                em.persist(product_1);
                PRODUCT_ID_1 = product_1.getId();

                Guide guide = new Guide(FILE_NAME_1);

                Product product_2 = new Product();
                product_2.getGuides().add(guide);
                guide.setProduct(product_2);

                em.persist(product_2);

                PRODUCT_ID_2 = product_2.getId();
                GUIDE_ID = guide.getId();

                tx.commit();
                em.close();
            }
            {
                tx.begin();

                EntityManager em = JPA.createEntityManager();

                Product product_1 = em.find(Product.class, PRODUCT_ID_1);
                assertTrue(product_1.getGuides().isEmpty());

                Product product_2 = em.find(Product.class, PRODUCT_ID_2);
                assertEquals(1, product_2.getGuides().size());

                Guide guide = em.find(Guide.class, GUIDE_ID);
                assertNotNull(guide.getProduct());

                tx.commit();
                em.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            TM.rollback();
        }
    }
}
