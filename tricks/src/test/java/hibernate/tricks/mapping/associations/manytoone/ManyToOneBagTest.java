package hibernate.tricks.mapping.associations.manytoone;

import hibernate.tricks.env.AbstractHibernateTest;
import hibernate.tricks.model.mapping.associations.manytoone.bag.Guide;
import hibernate.tricks.model.mapping.associations.manytoone.bag.Product;
import org.testng.annotations.Test;

import javax.persistence.EntityManager;
import javax.transaction.UserTransaction;

import static org.testng.AssertJUnit.assertEquals;
import static org.testng.AssertJUnit.assertSame;

public class ManyToOneBagTest extends AbstractHibernateTest {

    private static final String AUTHOR_1 = "AAA";
    private static final String AUTHOR_2 = "BBB";
    private static final String AUTHOR_3 = "CCC";

    private static final String FILE_NAME_1 = "FILE_NAME_1";
    private static final String FILE_NAME_2 = "FILE_NAME_2";
    private static final String FILE_NAME_3 = "FILE_NAME_3";

    private static final int EXPECTED_SIZE = 4;

    @Override
    public void configurePersistenceUnit() throws Exception {
        configurePersistenceUnit("ManyToOneBagPU");
    }

    @Test
    public void testManyToOneBag() {
        UserTransaction tx = TM.getUserTransaction();
        long PRODUCT_ID;

        try {
            {
                tx.begin();

                EntityManager em = JPA.createEntityManager();

                Product product = new Product();
                em.persist(product);
                PRODUCT_ID = product.getId();

                Guide guide_1 = new Guide(AUTHOR_1, FILE_NAME_1, product);
                Guide guide_2 = new Guide(AUTHOR_2, FILE_NAME_2, product);
                Guide guide_3 = new Guide(AUTHOR_3, FILE_NAME_3, product);

                product.getGuides().add(guide_1);
                product.getGuides().add(guide_2);
                product.getGuides().add(guide_3);
                //Дубликат будет добавлен в коллекцию, но не в базу
                product.getGuides().add(guide_3);

                assertEquals(EXPECTED_SIZE, product.getGuides().size());

                tx.commit();
                em.close();
            }
            {
                tx.begin();

                EntityManager em = JPA.createEntityManager();

                Product product = em.find(Product.class, PRODUCT_ID);

                //SELECT вызываться для вставки не будет
                //поэтому Collection более производителен чем Set
                Guide guide_1 = new Guide(AUTHOR_1, FILE_NAME_1, product);
                product.getGuides().add(guide_1);

                assertEquals(EXPECTED_SIZE, product.getGuides().size());

                Guide guide = em.find(Guide.class, PRODUCT_ID + 1);

                assertSame(product, guide.getProduct());

                tx.commit();
                em.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            TM.rollback();
        }
    }
}
