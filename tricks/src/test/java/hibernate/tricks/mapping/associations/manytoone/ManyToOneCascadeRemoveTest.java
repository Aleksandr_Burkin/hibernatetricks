package hibernate.tricks.mapping.associations.manytoone;

import hibernate.tricks.env.AbstractHibernateTest;
import hibernate.tricks.model.mapping.associations.manytoone.cascaderemove.Guide;
import hibernate.tricks.model.mapping.associations.manytoone.cascaderemove.Product;
import org.testng.annotations.Test;

import javax.persistence.EntityManager;
import javax.transaction.UserTransaction;
import java.util.List;

import static org.testng.AssertJUnit.assertEquals;
import static org.testng.AssertJUnit.assertSame;

public class ManyToOneCascadeRemoveTest extends AbstractHibernateTest {

    private static final String AUTHOR_1 = "AAA";
    private static final String AUTHOR_2 = "BBB";
    private static final String AUTHOR_3 = "CCC";

    private static final String FILE_NAME_1 = "FILE_NAME_1";
    private static final String FILE_NAME_2 = "FILE_NAME_2";
    private static final String FILE_NAME_3 = "FILE_NAME_3";

    private static final int EXPECTED_SIZE = 3;

    private static final String SQL = "SELECT g FROM Guide g";

    @Override
    public void configurePersistenceUnit() throws Exception {
        configurePersistenceUnit("ManyToOneCascadeRemovePU");
    }

    @Test
    public void testCascadeRemove() {
        UserTransaction tx = TM.getUserTransaction();
        long PRODUCT_ID;

        try {
            {
                tx.begin();

                EntityManager em = JPA.createEntityManager();

                Product product = new Product();
                em.persist(product);
                PRODUCT_ID = product.getId();

                Guide guide_1 = new Guide(AUTHOR_1, FILE_NAME_1, product);
                Guide guide_2 = new Guide(AUTHOR_2, FILE_NAME_2, product);
                Guide guide_3 = new Guide(AUTHOR_3, FILE_NAME_3, product);

                product.getGuides().add(guide_1);
                product.getGuides().add(guide_2);
                product.getGuides().add(guide_3);

                tx.commit();
                em.close();
            }
            {
                tx.begin();

                EntityManager em = JPA.createEntityManager();

                Product result = em.find(Product.class, PRODUCT_ID);
                assertEquals(EXPECTED_SIZE, result.getGuides().size());

                List list = em.createQuery(SQL).getResultList();

                assertSame(EXPECTED_SIZE, list.size());

                em.remove(result);

                tx.commit();
                em.close();
            }
            {
                tx.begin();

                EntityManager em = JPA.createEntityManager();

                List list = em.createQuery(SQL).getResultList();

                assertSame(0, list.size());

                tx.commit();
                em.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            TM.rollback();
        }
    }
}
