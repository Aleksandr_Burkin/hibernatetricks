package hibernate.tricks.mapping.associations.onetoone;

import hibernate.tricks.env.AbstractHibernateTest;
import hibernate.tricks.model.mapping.associations.onetoone.generalpk.Guide;
import hibernate.tricks.model.mapping.associations.onetoone.generalpk.Product;
import org.testng.annotations.Test;

import javax.persistence.EntityManager;
import javax.transaction.UserTransaction;

import static org.testng.AssertJUnit.assertSame;

public class OneToOneGeneralPkTest extends AbstractHibernateTest {

    private static final String FILE_NAME_1 = "FILE_NAME_1";

    @Override
    public void configurePersistenceUnit() throws Exception {
        configurePersistenceUnit("OneToOneGeneralPkPU");
    }

    @Test
    public void testOneToOneGeneralPk() {
        UserTransaction tx = TM.getUserTransaction();
        long PRODUCT_ID;

        try {
            {
                tx.begin();

                EntityManager em = JPA.createEntityManager();

                Product product = new Product();
                em.persist(product);
                PRODUCT_ID = product.getId();

                Guide guide = new Guide(PRODUCT_ID, FILE_NAME_1);
                guide.setProduct(product);
                em.persist(guide);

                tx.commit();
                em.close();
            }
            {
                tx.begin();

                EntityManager em = JPA.createEntityManager();

                Product product = em.find(Product.class, PRODUCT_ID);

                Guide guide = em.find(Guide.class, PRODUCT_ID);

                assertSame(product, guide.getProduct());

                tx.commit();
                em.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            TM.rollback();
        }
    }
}
