package hibernate.tricks.mapping.associations.manytoone;

import hibernate.tricks.env.AbstractHibernateTest;
import hibernate.tricks.model.mapping.associations.manytoone.set.Guide;
import hibernate.tricks.model.mapping.associations.manytoone.set.Product;
import org.testng.annotations.Test;

import javax.persistence.EntityManager;
import javax.transaction.UserTransaction;

import static org.testng.AssertJUnit.assertEquals;
import static org.testng.AssertJUnit.assertSame;

public class ManyToOneSetTest extends AbstractHibernateTest {

    private static final String AUTHOR_1 = "AAA";
    private static final String AUTHOR_2 = "BBB";
    private static final String AUTHOR_3 = "CCC";

    private static final String FILE_NAME_1 = "FILE_NAME_1";
    private static final String FILE_NAME_2 = "FILE_NAME_2";
    private static final String FILE_NAME_3 = "FILE_NAME_3";

    private static final int EXPECTED_SIZE = 3;

    @Override
    public void configurePersistenceUnit() throws Exception {
        configurePersistenceUnit("ManyToOneSetPU");
    }

    @Test
    public void testManyToOneSet() {
        UserTransaction tx = TM.getUserTransaction();
        long PRODUCT_ID;

        try {
            {
                tx.begin();

                EntityManager em = JPA.createEntityManager();

                Product product = new Product();
                em.persist(product);
                PRODUCT_ID = product.getId();

                Guide guide_1 = new Guide(AUTHOR_1, FILE_NAME_1, product);
                Guide guide_2 = new Guide(AUTHOR_2, FILE_NAME_2, product);
                Guide guide_3 = new Guide(AUTHOR_3, FILE_NAME_3, product);
                Guide guide_33 = new Guide(AUTHOR_3, FILE_NAME_3, product);

                product.getGuides().add(guide_1);
                product.getGuides().add(guide_2);
                product.getGuides().add(guide_3);
                //Дубликат не добавиться
                product.getGuides().add(guide_33);

                assertEquals(EXPECTED_SIZE, product.getGuides().size());

                tx.commit();
                em.close();
            }
            {
                tx.begin();

                EntityManager em = JPA.createEntityManager();

                Product product = em.find(Product.class, PRODUCT_ID);

                //SELECT будет вызван перед вставкой для проверки новых записей на уникальность
                //поэтому Set менее производителен чем Collection
                Guide guide_1 = new Guide(AUTHOR_1, FILE_NAME_1, product);
                product.getGuides().add(guide_1);

                assertEquals(EXPECTED_SIZE, product.getGuides().size());

                Guide guide = em.find(Guide.class, PRODUCT_ID + 1);

                assertSame(product, guide.getProduct());

                tx.commit();
                em.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            TM.rollback();
        }
    }
}
