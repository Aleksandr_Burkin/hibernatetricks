package hibernate.tricks.mapping.associations.manytoone;

import hibernate.tricks.env.AbstractHibernateTest;
import hibernate.tricks.model.mapping.associations.manytoone.list.Guide;
import hibernate.tricks.model.mapping.associations.manytoone.list.Product;
import org.testng.annotations.Test;

import javax.persistence.EntityManager;
import javax.transaction.UserTransaction;
import java.util.List;

import static org.testng.AssertJUnit.assertEquals;
import static org.testng.AssertJUnit.assertSame;

public class ManyToOneListTest extends AbstractHibernateTest {

    private static final String AUTHOR_1 = "AAA";
    private static final String AUTHOR_2 = "BBB";
    private static final String AUTHOR_3 = "CCC";

    private static final String FILE_NAME_1 = "FILE_NAME_1";
    private static final String FILE_NAME_2 = "FILE_NAME_2";
    private static final String FILE_NAME_3 = "FILE_NAME_3";

    private static final int EXPECTED_SIZE = 4;

    private static final String HQL = "SELECT g FROM Guide g";
    private static final String SQL = "SELECT guide_position FROM guide";

    @Override
    public void configurePersistenceUnit() throws Exception {
        configurePersistenceUnit("ManyToOneListPU");
    }

    @Test
    public void testManyToOneList() {
        UserTransaction tx = TM.getUserTransaction();
        long PRODUCT_ID;

        try {
            {
                tx.begin();

                EntityManager em = JPA.createEntityManager();

                Product product = new Product();
                em.persist(product);
                PRODUCT_ID = product.getId();

                Guide guide_1 = new Guide(AUTHOR_1, FILE_NAME_1, product);
                Guide guide_2 = new Guide(AUTHOR_2, FILE_NAME_2, product);
                Guide guide_3 = new Guide(AUTHOR_3, FILE_NAME_3, product);
                Guide guide_33 = new Guide(AUTHOR_3, FILE_NAME_3, product);

                product.getGuides().add(guide_1);
                product.getGuides().add(guide_2);
                product.getGuides().add(guide_3);
                //добавиться в БД т.к. теперь есть дополнительная колонка для хранения позиции
                product.getGuides().add(guide_33);

                assertEquals(EXPECTED_SIZE, product.getGuides().size());

                tx.commit();
                em.close();
            }
            {
                tx.begin();

                EntityManager em = JPA.createEntityManager();

                Product product = em.find(Product.class, PRODUCT_ID);

                Guide guide_1 = new Guide(AUTHOR_1, FILE_NAME_1, product);
                product.getGuides().add(guide_1);

                assertEquals(EXPECTED_SIZE + 1, product.getGuides().size());

                Guide guide = em.find(Guide.class, PRODUCT_ID + 1);

                assertSame(product, guide.getProduct());

                tx.commit();
                em.close();
            }
            {
                tx.begin();

                EntityManager em = JPA.createEntityManager();

                //название Entity чувствительно к регистру
                //через HQL не видно дополнительной колонки
                List<Guide> guides = em.createQuery(HQL).getResultList();

                List resultList = em.createNativeQuery(SQL).getResultList();
                assertEquals(EXPECTED_SIZE + 1, resultList.size());

                tx.commit();
                em.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            TM.rollback();
        }
    }
}
