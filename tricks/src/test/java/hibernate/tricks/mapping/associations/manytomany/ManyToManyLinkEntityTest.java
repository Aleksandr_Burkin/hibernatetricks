package hibernate.tricks.mapping.associations.manytomany;

import hibernate.tricks.env.AbstractHibernateTest;
import hibernate.tricks.model.mapping.associations.manytomany.linkentity.Author;
import hibernate.tricks.model.mapping.associations.manytomany.linkentity.AuthorGuide;
import hibernate.tricks.model.mapping.associations.manytomany.linkentity.Guide;
import org.testng.annotations.Test;

import javax.persistence.EntityManager;
import javax.transaction.UserTransaction;
import java.util.Set;

import static org.testng.AssertJUnit.assertEquals;

public class ManyToManyLinkEntityTest extends AbstractHibernateTest {

    private static final String AUTHOR_NAME_1 = "AUTHOR_NAME_1";
    private static final String AUTHOR_NAME_2 = "AUTHOR_NAME_2";

    private static final String TITLE_1 = "TITLE_1";
    private static final String TITLE_2 = "TITLE_2";

    private static final String ADDED_BY_1 = "ADDED_BY_1";
    private static final String ADDED_BY_2 = "ADDED_BY_2";
    private static final String ADDED_BY_3 = "ADDED_BY_3";

    @Override
    public void configurePersistenceUnit() throws Exception {
        configurePersistenceUnit("ManyToManyLinkEntityPU");
    }

    @Test
    public void testManyToManyLinkEntity() {
        UserTransaction tx = TM.getUserTransaction();
        long AUTHOR_ID_1, AUTHOR_ID_2;

        AuthorGuide.Id AUTHOR_GUIDE_ID_1, AUTHOR_GUIDE_ID_2, AUTHOR_GUIDE_ID_3;

        try {
            {
                tx.begin();

                EntityManager em = JPA.createEntityManager();

                Author author_1 = new Author(AUTHOR_NAME_1);
                Author author_2 = new Author(AUTHOR_NAME_2);
                em.persist(author_1);
                em.persist(author_2);

                Guide guide_1 = new Guide(TITLE_1);
                Guide guide_2 = new Guide(TITLE_2);
                em.persist(guide_1);
                em.persist(guide_2);

                AuthorGuide link_1 = new AuthorGuide(ADDED_BY_1, author_1, guide_1);
                AuthorGuide link_2 = new AuthorGuide(ADDED_BY_2, author_2, guide_1);
                AuthorGuide link_3 = new AuthorGuide(ADDED_BY_3, author_1, guide_2);

                em.persist(link_1);
                em.persist(link_2);
                em.persist(link_3);

                AUTHOR_ID_1 = author_1.getId();
                AUTHOR_ID_2 = author_2.getId();

                AUTHOR_GUIDE_ID_1 = link_1.getId();
                AUTHOR_GUIDE_ID_2 = link_2.getId();
                AUTHOR_GUIDE_ID_3 = link_3.getId();

                tx.commit();
                em.close();
            }
            {
                tx.begin();

                EntityManager em = JPA.createEntityManager();

                AuthorGuide authorGuide_1 = em.find(AuthorGuide.class, AUTHOR_GUIDE_ID_1);
                assertEquals(ADDED_BY_1, authorGuide_1.getAddedBy());
                Author author_1 = authorGuide_1.getAuthor();
                assertEquals(AUTHOR_ID_1, author_1.getId().longValue());
                Set<AuthorGuide> authorGuides_1 = author_1.getAuthorGuides();
                assertEquals(2, authorGuides_1.size());

                AuthorGuide authorGuide_2 = em.find(AuthorGuide.class, AUTHOR_GUIDE_ID_2);
                assertEquals(ADDED_BY_2, authorGuide_2.getAddedBy());
                Author author_2 = authorGuide_2.getAuthor();
                assertEquals(AUTHOR_ID_2, author_2.getId().longValue());
                Set<AuthorGuide> authorGuides_2 = author_2.getAuthorGuides();
                assertEquals(1, authorGuides_2.size());

                AuthorGuide authorGuide_3 = em.find(AuthorGuide.class, AUTHOR_GUIDE_ID_3);
                assertEquals(ADDED_BY_3, authorGuide_3.getAddedBy());
                Author author_3 = authorGuide_3.getAuthor();
                assertEquals(AUTHOR_ID_1, author_3.getId().longValue());
                Set<AuthorGuide> authorGuides_3 = author_3.getAuthorGuides();
                assertEquals(2, authorGuides_3.size());

                tx.commit();
                em.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            TM.rollback();
        }
    }
}
