package hibernate.tricks.mapping.associations.onetoone;

import hibernate.tricks.env.AbstractHibernateTest;
import hibernate.tricks.model.mapping.associations.onetoone.foreignkey.Guide;
import hibernate.tricks.model.mapping.associations.onetoone.foreignkey.Product;
import org.testng.annotations.Test;

import javax.persistence.EntityManager;
import javax.transaction.UserTransaction;

import static org.testng.AssertJUnit.assertSame;

public class OneToOneForeignKeyTest extends AbstractHibernateTest {

    private static final String FILE_NAME_1 = "FILE_NAME_1";

    @Override
    public void configurePersistenceUnit() throws Exception {
        configurePersistenceUnit("OneToOneForeignKeyPU");
    }

    @Test
    public void testOneToOneForeignKey() {
        UserTransaction tx = TM.getUserTransaction();
        long PRODUCT_ID;
        long GUIDE_ID;

        try {
            {
                tx.begin();

                EntityManager em = JPA.createEntityManager();

                Product product = new Product();
                Guide guide = new Guide(FILE_NAME_1);
                product.setGuide(guide);

                em.persist(product);
                PRODUCT_ID = product.getId();
                GUIDE_ID = guide.getId();

                tx.commit();
                em.close();
            }
            {
                tx.begin();

                EntityManager em = JPA.createEntityManager();

                Product product = em.find(Product.class, PRODUCT_ID);
                Guide guide = em.find(Guide.class, GUIDE_ID);
                assertSame(product.getGuide(), guide);

                tx.commit();
                em.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            TM.rollback();
        }
    }
}
