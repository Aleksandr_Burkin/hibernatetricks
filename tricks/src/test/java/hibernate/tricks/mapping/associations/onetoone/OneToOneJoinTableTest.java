package hibernate.tricks.mapping.associations.onetoone;

import hibernate.tricks.env.AbstractHibernateTest;
import hibernate.tricks.model.mapping.associations.onetoone.jointable.Guide;
import hibernate.tricks.model.mapping.associations.onetoone.jointable.Product;
import org.testng.annotations.Test;

import javax.persistence.EntityManager;
import javax.transaction.UserTransaction;

import static org.testng.AssertJUnit.assertNull;
import static org.testng.AssertJUnit.assertSame;

public class OneToOneJoinTableTest extends AbstractHibernateTest {

    private static final String FILE_NAME_1 = "FILE_NAME_1";

    @Override
    public void configurePersistenceUnit() throws Exception {
        configurePersistenceUnit("OneToOneJoinTablePU");
    }

    @Test
    public void testOneToOneAutoGeneral() {
        UserTransaction tx = TM.getUserTransaction();
        long PRODUCT_ID_1;
        long PRODUCT_ID_2;
        long GUIDE_ID;

        try {
            {
                tx.begin();

                EntityManager em = JPA.createEntityManager();

                Product product_1 = new Product();
                em.persist(product_1);
                PRODUCT_ID_1 = product_1.getId();

                Guide guide = new Guide(FILE_NAME_1);
                em.persist(guide);
                GUIDE_ID = guide.getId();

                Product product_2 = new Product(guide);
                em.persist(product_2);
                PRODUCT_ID_2 = product_2.getId();

                tx.commit();
                em.close();
            }
            {
                tx.begin();

                EntityManager em = JPA.createEntityManager();

                Product product_1 = em.find(Product.class, PRODUCT_ID_1);
                assertNull(product_1.getGuide());

                Product product_2 = em.find(Product.class, PRODUCT_ID_2);
                Guide guide = em.find(Guide.class, GUIDE_ID);
                assertSame(guide, product_2.getGuide());

                tx.commit();
                em.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            TM.rollback();
        }
    }
}
