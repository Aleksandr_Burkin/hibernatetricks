package hibernate.tricks.mapping.inheritance;

import hibernate.tricks.env.AbstractHibernateTest;
import hibernate.tricks.model.mapping.inheritance.BuyerPolymorphic;
import hibernate.tricks.model.mapping.inheritance.joined.CarJoined;
import hibernate.tricks.model.mapping.inheritance.joined.ProductJoined;
import org.testng.annotations.Test;

import javax.persistence.EntityManager;
import javax.transaction.UserTransaction;

import static org.testng.AssertJUnit.assertEquals;
import static org.testng.AssertJUnit.assertFalse;
import static org.testng.AssertJUnit.assertNotSame;

public class PolymorphicTest extends AbstractHibernateTest {

    private static final String TEST_CAR_BRAND = "CAR_BRAND";
    private static final String TEST_CAR_ENGINE = "ENGINE";
    private static final Integer TEST_CAR_PASSENGERS = 4;

    @Override
    public void configurePersistenceUnit() throws Exception {
        configurePersistenceUnit("InheritancePU");
    }

    @Test
    public void testPolymorphic() {
        try {
            {
                UserTransaction tx = TM.getUserTransaction();
                tx.begin();

                EntityManager em = JPA.createEntityManager();

                BuyerPolymorphic buyer = new BuyerPolymorphic();

                CarJoined car = new CarJoined();
                car.setBrand(TEST_CAR_BRAND);
                car.setEngine(TEST_CAR_ENGINE);
                car.setPassengers(TEST_CAR_PASSENGERS);
                buyer.setProduct(car);

                em.persist(car);
                em.persist(buyer);

                tx.commit();
                em.close();
            }

            {
                UserTransaction tx = TM.getUserTransaction();
                tx.begin();

                EntityManager em = JPA.createEntityManager();

                BuyerPolymorphic buyer = em.find(BuyerPolymorphic.class, 2L);

                ProductJoined product = buyer.getProduct();

                assertFalse(product instanceof CarJoined);
                assertEquals(TEST_CAR_BRAND, product.getBrand());

                CarJoined car = em.getReference(CarJoined.class, product.getId());

                assertNotSame(product, car);

                assertEquals(TEST_CAR_ENGINE, car.getEngine());
                assertEquals(TEST_CAR_PASSENGERS, car.getPassengers());

                tx.commit();
                em.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            TM.rollback();
        }
    }
}
