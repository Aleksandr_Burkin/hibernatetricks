package hibernate.tricks.mapping.inheritance;

import hibernate.tricks.env.AbstractHibernateTest;
import hibernate.tricks.model.mapping.inheritance.singletable.mixed.CarMixed;
import org.testng.annotations.Test;

import javax.persistence.EntityManager;
import javax.transaction.UserTransaction;

import static org.testng.AssertJUnit.assertEquals;

public class MixedTest extends AbstractHibernateTest {

    private static final String TEST_CAR_BRAND = "CAR_BRAND";
    private static final String TEST_CAR_ENGINE = "ENGINE";
    private static final Integer TEST_CAR_PASSENGERS = 4;

    @Override
    public void configurePersistenceUnit() throws Exception {
        configurePersistenceUnit("InheritancePU");
    }

    @Test
    public void testMixed() {
        try {
            {
                UserTransaction tx = TM.getUserTransaction();
                tx.begin();

                EntityManager em = JPA.createEntityManager();

                CarMixed car = new CarMixed();
                car.setBrand(TEST_CAR_BRAND);
                car.setEngine(TEST_CAR_ENGINE);
                car.setPassengers(TEST_CAR_PASSENGERS);

                em.persist(car);

                tx.commit();
                em.close();
            }

            {
                UserTransaction tx = TM.getUserTransaction();
                tx.begin();

                EntityManager em = JPA.createEntityManager();

                CarMixed car = em.find(CarMixed.class, 1L);

                assertEquals(TEST_CAR_BRAND, car.getBrand());
                assertEquals(TEST_CAR_ENGINE, car.getEngine());
                assertEquals(TEST_CAR_PASSENGERS, car.getPassengers());

                tx.commit();
                em.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            TM.rollback();
        }
    }
}
