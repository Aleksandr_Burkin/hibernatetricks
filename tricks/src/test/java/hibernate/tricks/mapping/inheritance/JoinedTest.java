package hibernate.tricks.mapping.inheritance;

import hibernate.tricks.env.AbstractHibernateTest;
import hibernate.tricks.model.mapping.inheritance.joined.CarJoined;
import hibernate.tricks.model.mapping.inheritance.joined.ClothesJoined;
import org.testng.annotations.Test;

import javax.persistence.EntityManager;
import javax.transaction.UserTransaction;

import static org.testng.AssertJUnit.assertEquals;

public class JoinedTest extends AbstractHibernateTest {

    private static final String TEST_CAR_BRAND = "CAR_BRAND";
    private static final String TEST_CAR_ENGINE = "ENGINE";
    private static final Integer TEST_CAR_PASSENGERS = 4;

    private static final String TEST_CLOTHES_BRAND = "CLOTHES_BRAND";
    private static final String TEST_CLOTHES_TEXTILE = "TEXTILE";
    private static final Integer TEST_CLOTHES_SIZE = 50;

    @Override
    public void configurePersistenceUnit() throws Exception {
        configurePersistenceUnit("InheritancePU");
    }

    @Test
    public void testMappedSuperclass() {
        try {
            {
                UserTransaction tx = TM.getUserTransaction();
                tx.begin();

                EntityManager em = JPA.createEntityManager();

                CarJoined car = new CarJoined();
                car.setBrand(TEST_CAR_BRAND);
                car.setEngine(TEST_CAR_ENGINE);
                car.setPassengers(TEST_CAR_PASSENGERS);

                ClothesJoined clothes = new ClothesJoined();
                clothes.setBrand(TEST_CLOTHES_BRAND);
                clothes.setTextile(TEST_CLOTHES_TEXTILE);
                clothes.setSize(TEST_CLOTHES_SIZE);

                em.persist(car);
                em.persist(clothes);

                tx.commit();
                em.close();
            }

            {
                UserTransaction tx = TM.getUserTransaction();
                tx.begin();

                EntityManager em = JPA.createEntityManager();

                CarJoined car = em.find(CarJoined.class, 1L);
                ClothesJoined clothes = em.find(ClothesJoined.class, 2L);

                assertEquals(TEST_CAR_BRAND, car.getBrand());
                assertEquals(TEST_CAR_ENGINE, car.getEngine());
                assertEquals(TEST_CAR_PASSENGERS, car.getPassengers());

                assertEquals(TEST_CLOTHES_BRAND, clothes.getBrand());
                assertEquals(TEST_CLOTHES_TEXTILE, clothes.getTextile());
                assertEquals(TEST_CLOTHES_SIZE, clothes.getSize());

                tx.commit();
                em.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            TM.rollback();
        }
    }
}
