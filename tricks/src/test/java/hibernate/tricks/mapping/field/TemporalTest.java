package hibernate.tricks.mapping.field;

import hibernate.tricks.env.AbstractHibernateTest;
import hibernate.tricks.model.mapping.fields.ProductTemporal;
import org.testng.annotations.Test;

import javax.persistence.EntityManager;
import javax.transaction.UserTransaction;

import static org.testng.AssertJUnit.assertNotNull;

public class TemporalTest extends AbstractHibernateTest {

    @Override
    public void configurePersistenceUnit() throws Exception {
        configurePersistenceUnit("BaseTypeFieldPU");
    }

    @Test
    public void testTemporal() {
        try {
            {
                UserTransaction tx = TM.getUserTransaction();
                tx.begin();

                EntityManager em = JPA.createEntityManager();

                em.persist(new ProductTemporal());

                tx.commit();
                em.close();
            }

            {
                UserTransaction tx = TM.getUserTransaction();
                tx.begin();

                EntityManager em = JPA.createEntityManager();

                ProductTemporal result = em.find(ProductTemporal.class, 1L);

                assertNotNull(result.getCreatedData());

                tx.commit();
                em.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            TM.rollback();
        }
    }
}
