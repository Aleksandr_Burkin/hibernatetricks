package hibernate.tricks.mapping.field;

import hibernate.tricks.env.AbstractHibernateTest;
import hibernate.tricks.model.mapping.fields.ProductBlob;
import org.hibernate.Session;
import org.hibernate.engine.jdbc.StreamUtils;
import org.testng.annotations.Test;

import javax.persistence.EntityManager;
import javax.transaction.UserTransaction;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.sql.Blob;
import java.util.Random;

import static org.testng.Assert.assertEquals;

public class BlobHibernateTest extends AbstractHibernateTest {

    private static final int SIZE = 131072;

    @Override
    public void configurePersistenceUnit() throws Exception {
        configurePersistenceUnit("BaseTypeFieldPU");
    }

    @Test
    public void storeLoadLocator() throws Exception {
        UserTransaction tx = TM.getUserTransaction();
        try {
            tx.begin();

            EntityManager em = JPA.createEntityManager();

            byte[] bytes = new byte[SIZE];
            new Random().nextBytes(bytes);
            InputStream is = new ByteArrayInputStream(bytes);
            int byteLength = bytes.length;

            ProductBlob product = new ProductBlob();

            //native Hibernate API
            Session session = em.unwrap(Session.class);
            Blob blob = session.getLobHelper().createBlob(is, byteLength);

            product.setBlob(blob);
            em.persist(product);

            tx.commit();
            em.close();

            Long ID = product.getId();

            tx.begin();
            em = JPA.createEntityManager();

            product = em.find(ProductBlob.class, ID);

            InputStream inStream = product.getBlob().getBinaryStream();
            ByteArrayOutputStream outStream = new ByteArrayOutputStream();
            StreamUtils.copy(inStream, outStream);
            byte[] imageBytes = outStream.toByteArray();

            assertEquals(imageBytes.length, SIZE);

            tx.commit();
            em.close();
        } finally {
            TM.rollback();
        }
    }
}
