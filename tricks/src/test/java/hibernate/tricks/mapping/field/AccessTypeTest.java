package hibernate.tricks.mapping.field;

import hibernate.tricks.env.AbstractHibernateTest;
import hibernate.tricks.model.mapping.fields.ProductAccessType;
import org.testng.annotations.Test;

import javax.persistence.EntityManager;
import javax.transaction.UserTransaction;

import java.lang.reflect.Field;

import static org.testng.Assert.assertNotEquals;
import static org.testng.AssertJUnit.assertEquals;
import static org.testng.AssertJUnit.assertNotNull;

public class AccessTypeTest extends AbstractHibernateTest {

    private static final String TEST_NAME = "TEST_NAME";
    private static final String EXPECTED_NAME_ACCESS_FIELD = "ACCESS_FIELD";
    private static final String EXPECTED_NAME_ACCESS_PROPERTY = "ACCESS_PROPERTY";

    @Override
    public void configurePersistenceUnit() throws Exception {
        configurePersistenceUnit("BaseTypeFieldPU");
    }

    @Test
    public void testAccessType() {
        try {
            {
                UserTransaction tx = TM.getUserTransaction();
                tx.begin();

                EntityManager em = JPA.createEntityManager();

                ProductAccessType product_1 = new ProductAccessType();
                product_1.setNameAccessField(TEST_NAME);
                product_1.setNameAccessProperty(TEST_NAME);

                em.persist(product_1);

                tx.commit();
                em.close();
            }

            {
                UserTransaction tx = TM.getUserTransaction();
                tx.begin();

                EntityManager em = JPA.createEntityManager();

                ProductAccessType result = em.find(ProductAccessType.class, 1L);

                Field nameAccessField = result.getClass().getDeclaredField("nameAccessField");
                nameAccessField.setAccessible(true);
                String accessFieldValue = (String) nameAccessField.get(result);

                Field nameAccessProperty = result.getClass().getDeclaredField("nameAccessProperty");
                nameAccessProperty.setAccessible(true);
                String accessPropertyValue = (String) nameAccessProperty.get(result);

                assertNotNull(result);
                assertNotEquals(EXPECTED_NAME_ACCESS_FIELD, accessFieldValue);

                //!!!
                assertEquals(EXPECTED_NAME_ACCESS_PROPERTY, accessPropertyValue);

                tx.commit();
                em.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            TM.rollback();
        }
    }
}
