package hibernate.tricks.mapping.field;

import hibernate.tricks.env.AbstractHibernateTest;
import hibernate.tricks.model.mapping.fields.ProductColumnTransformer;
import org.testng.annotations.Test;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.transaction.UserTransaction;
import java.util.List;

import static org.testng.AssertJUnit.assertEquals;

public class ColumnTransformerTest extends AbstractHibernateTest {

    private static final double TEST_VALUE = 100;
    private static final double EXPECTED_TEST_VALUE = 100;
    private static final double NATIVE_TEST_VALUE = 220.462;

    private static final String SQL = "select weight from ProductColumnTransformer";

    @Override
    public void configurePersistenceUnit() throws Exception {
        configurePersistenceUnit("BaseTypeFieldPU");
    }

    @Test
    public void testColumnTransformer() {
        try {
            {
                UserTransaction tx = TM.getUserTransaction();
                tx.begin();

                EntityManager em = JPA.createEntityManager();

                ProductColumnTransformer product_1 = new ProductColumnTransformer();
                product_1.setWeight(TEST_VALUE);

                em.persist(product_1);

                tx.commit();
                em.close();
            }

            {
                UserTransaction tx = TM.getUserTransaction();
                tx.begin();

                EntityManager em = JPA.createEntityManager();

                ProductColumnTransformer result = em.find(ProductColumnTransformer.class, 1L);

                assertEquals(EXPECTED_TEST_VALUE, result.getWeight());

                Query nativeQuery = em.createNativeQuery(SQL);
                List resultList = nativeQuery.getResultList();

                assertEquals(NATIVE_TEST_VALUE, resultList.get(0));

                tx.commit();
                em.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            TM.rollback();
        }
    }
}
