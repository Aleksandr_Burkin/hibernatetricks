package hibernate.tricks.mapping.field;

import hibernate.tricks.env.AbstractHibernateTest;
import hibernate.tricks.model.mapping.fields.ProductTransient;
import org.testng.annotations.Test;

import javax.persistence.EntityManager;
import javax.transaction.UserTransaction;

import static org.testng.Assert.assertNotEquals;
import static org.testng.AssertJUnit.assertNull;
import static org.testng.AssertJUnit.assertNotNull;

public class TransientTest extends AbstractHibernateTest {

    private static final String TEST_NAME = "Test_Name";

    @Override
    public void configurePersistenceUnit() throws Exception {
        configurePersistenceUnit("BaseTypeFieldPU");
    }

    @Test
    public void testTransientField() {
        try {
            {
                UserTransaction tx = TM.getUserTransaction();
                tx.begin();

                EntityManager em = JPA.createEntityManager();

                ProductTransient product_1 = new ProductTransient();
                product_1.setName(TEST_NAME);

                em.persist(product_1);

                tx.commit();
                em.close();
            }

            {
                UserTransaction tx = TM.getUserTransaction();
                tx.begin();

                EntityManager em = JPA.createEntityManager();

                ProductTransient result = em.find(ProductTransient.class, 1L);

                assertNotNull(result);
                assertNotEquals(result.getName(), TEST_NAME);
                assertNull(result.getName());

                tx.commit();
                em.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            TM.rollback();
        }
    }
}
