package hibernate.tricks.mapping.field;

import hibernate.tricks.env.AbstractHibernateTest;
import hibernate.tricks.model.mapping.fields.LevelType;
import hibernate.tricks.model.mapping.fields.ProductEnum;
import org.testng.annotations.Test;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.transaction.UserTransaction;

import static org.testng.AssertJUnit.assertEquals;

public class EnumTest extends AbstractHibernateTest {

    private static final String ORDINAL_SQL = "select levelTypeOrdinal from ProductEnum";
    private static final String STRING_SQL = "select levelTypeString from ProductEnum";

    @Override
    public void configurePersistenceUnit() throws Exception {
        configurePersistenceUnit("BaseTypeFieldPU");
    }

    @Test
    public void testEnum() {
        try {
            {
                UserTransaction tx = TM.getUserTransaction();
                tx.begin();

                EntityManager em = JPA.createEntityManager();

                em.persist(new ProductEnum());

                tx.commit();
                em.close();
            }

            {
                UserTransaction tx = TM.getUserTransaction();
                tx.begin();

                EntityManager em = JPA.createEntityManager();

                ProductEnum result = em.find(ProductEnum.class, 1L);

                assertEquals(LevelType.HIGH, result.getLevelTypeOrdinal());
                assertEquals(LevelType.HIGH, result.getLevelTypeString());

                Query nativeQueryOrdinal = em.createNativeQuery(ORDINAL_SQL);
                assertEquals(0, nativeQueryOrdinal.getResultList().get(0));

                Query nativeQueryString = em.createNativeQuery(STRING_SQL);
                assertEquals(LevelType.HIGH.toString(), nativeQueryString.getResultList().get(0));

                tx.commit();
                em.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            TM.rollback();
        }
    }
}
