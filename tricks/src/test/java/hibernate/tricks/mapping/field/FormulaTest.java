package hibernate.tricks.mapping.field;

import hibernate.tricks.env.AbstractHibernateTest;
import hibernate.tricks.model.mapping.fields.Product;
import org.testng.annotations.Test;

import javax.persistence.EntityManager;
import javax.transaction.UserTransaction;

import static org.testng.AssertJUnit.assertEquals;

public class FormulaTest extends AbstractHibernateTest {

    private static final String TEST_NAME = "Test_Name";
    private static final String EXPECTED_TEST_NAME = "Test_Nickname";

    @Override
    public void configurePersistenceUnit() throws Exception {
        configurePersistenceUnit("BaseTypeFieldPU");
    }

    @Test
    public void testFormula() {
        try {
            {
                UserTransaction tx = TM.getUserTransaction();
                tx.begin();

                EntityManager em = JPA.createEntityManager();

                Product product_1 = new Product();
                product_1.setName(TEST_NAME);

                em.persist(product_1);

                tx.commit();
                em.close();
            }

            {
                UserTransaction tx = TM.getUserTransaction();
                tx.begin();

                EntityManager em = JPA.createEntityManager();

                Product result = em.find(Product.class, 1L);

                assertEquals(EXPECTED_TEST_NAME, result.getNickName());

                tx.commit();
                em.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            TM.rollback();
        }
    }
}
