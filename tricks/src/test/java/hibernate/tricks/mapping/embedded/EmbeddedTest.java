package hibernate.tricks.mapping.embedded;

import hibernate.tricks.env.AbstractHibernateTest;
import hibernate.tricks.model.mapping.embedded.Address;
import hibernate.tricks.model.mapping.embedded.City;
import hibernate.tricks.model.mapping.embedded.ProductEmbedded;
import org.testng.annotations.Test;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.transaction.UserTransaction;

import static org.testng.Assert.assertNull;
import static org.testng.AssertJUnit.assertEquals;

public class EmbeddedTest extends AbstractHibernateTest {

    private static final String STORAGE_COUNTRY = "Россиийская Федерация";
    private static final String STORAGE_CITY = "Москва";
    private static final String STORAGE_STREET = "ул. Ленина";

    private static final String DELIVERY_COUNTRY = "Республика Беларусь";
    private static final String DELIVERY_CITY = "Минск";
    private static final String DELIVERY_STREET = "ул. Карла Маркса";

    private static final String SQL = "select delivery_country, delivery_city, delivery_street from ProductEmbedded";

    @Override
    public void configurePersistenceUnit() throws Exception {
        configurePersistenceUnit("EmbeddedPU");
    }

    @Test
    public void testEmbedded() {
        try {
            {
                UserTransaction tx = TM.getUserTransaction();
                tx.begin();

                EntityManager em = JPA.createEntityManager();

                ProductEmbedded product = new ProductEmbedded();
                Address storage = new Address();
                storage.setCountry(STORAGE_COUNTRY);
                City city = new City();
                city.setName(STORAGE_CITY);
                city.setStreet(STORAGE_STREET);
                storage.setCity(city);
                product.setStorage(storage);

                Address delivery = new Address();
                delivery.setCountry(DELIVERY_COUNTRY);
                City cityDelivery = new City();
                cityDelivery.setName(DELIVERY_CITY);
                cityDelivery.setStreet(DELIVERY_STREET);
                delivery.setCity(cityDelivery);
                product.setDelivery(delivery);


                em.persist(product);

                tx.commit();
                em.close();
            }

            {
                UserTransaction tx = TM.getUserTransaction();
                tx.begin();

                EntityManager em = JPA.createEntityManager();

                ProductEmbedded result = em.find(ProductEmbedded.class, 1L);

                assertEquals(STORAGE_COUNTRY, result.getStorage().getCountry());
                assertEquals(STORAGE_CITY, result.getStorage().getCity().getName());
                assertEquals(STORAGE_STREET, result.getStorage().getCity().getStreet());

                Query nativeQuery = em.createNativeQuery(SQL);
                Object[] row = (Object[]) nativeQuery.getResultList().get(0);

                assertEquals(DELIVERY_COUNTRY, row[0]);
                assertEquals(DELIVERY_CITY, row[1]);
                assertEquals(DELIVERY_STREET, row[2]);

                tx.commit();
                em.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            TM.rollback();
        }
    }

    @Test
    public void testEmbedded_null() {
        try {
            {
                UserTransaction tx = TM.getUserTransaction();
                tx.begin();

                EntityManager em = JPA.createEntityManager();

                ProductEmbedded product = new ProductEmbedded();
                product.setStorage(new Address());
                product.setDelivery(new Address());

                em.persist(product);

                tx.commit();
                em.close();
            }

            {
                UserTransaction tx = TM.getUserTransaction();
                tx.begin();

                EntityManager em = JPA.createEntityManager();

                ProductEmbedded result = em.find(ProductEmbedded.class, 1L);

                assertNull(result.getStorage());
                assertNull(result.getDelivery());

                Query nativeQuery = em.createNativeQuery(SQL);
                Object[] results = (Object[]) nativeQuery.getResultList().get(0);

                assertNull(results[0]);
                assertNull(results[1]);
                assertNull(results[2]);

                tx.commit();
                em.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            TM.rollback();
        }
    }
}
