package hibernate.tricks.mapping.pk;

import hibernate.tricks.env.AbstractHibernateTest;
import hibernate.tricks.model.mapping.pk.ProductPK;
import org.testng.annotations.Test;

import javax.persistence.EntityManager;
import javax.transaction.*;

import static org.testng.AssertJUnit.assertEquals;

public class HibernateStrategyPkGeneration extends AbstractHibernateTest {

    @Override
    public void configurePersistenceUnit() throws Exception {
        configurePersistenceUnit("HibernateStrategyPkGenerationPU");
    }

    @Test
    public void testPkGeneration() {
        UserTransaction tx = TM.getUserTransaction();

        try {
            tx.begin();

            EntityManager em = JPA.createEntityManager();

            ProductPK product_1 = new ProductPK();
            ProductPK product_2 = new ProductPK();

            em.persist(product_1);
            em.persist(product_2);

            assertEquals(product_1.getId().longValue(), 1000L);
            assertEquals(product_2.getId().longValue(), 1001L);

            tx.commit();
            em.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            TM.rollback();
        }
    }
}
