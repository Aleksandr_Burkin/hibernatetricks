package hibernate.tricks.mapping.collections;

import hibernate.tricks.env.AbstractHibernateTest;
import hibernate.tricks.model.mapping.collections.setofembeddables.Guide;
import hibernate.tricks.model.mapping.collections.setofembeddables.Product;
import org.testng.annotations.Test;

import javax.persistence.EntityManager;
import javax.transaction.UserTransaction;
import java.util.Iterator;
import java.util.Set;

import static org.testng.AssertJUnit.assertEquals;

public class SetOfEmbeddablesTest extends AbstractHibernateTest {

    private static final String AUTHOR_1 = "AAA";
    private static final String AUTHOR_2 = "BBB";
    private static final String AUTHOR_3 = "CCC";

    private static final String FILE_NAME_1 = "FILE_NAME_1";
    private static final String FILE_NAME_2 = "FILE_NAME_2";
    private static final String FILE_NAME_3 = "FILE_NAME_3";

    private static final int EXPECTED_SIZE = 3;

    @Override
    public void configurePersistenceUnit() throws Exception {
        configurePersistenceUnit("SetOfEmbeddablesPU");
    }

    @Test
    public void testSetOfEmbeddables() {
        UserTransaction tx = TM.getUserTransaction();
        long PRODUCT_ID;

        try {
            {
                tx.begin();

                EntityManager em = JPA.createEntityManager();

                Product product = new Product();
                em.persist(product);
                PRODUCT_ID = product.getId();

                product.getGuides().add(new Guide(AUTHOR_1, FILE_NAME_1));
                product.getGuides().add(new Guide(AUTHOR_2, FILE_NAME_2));
                product.getGuides().add(new Guide(AUTHOR_3, FILE_NAME_3));

                tx.commit();
                em.close();
            }

            {
                tx.begin();

                EntityManager em = JPA.createEntityManager();

                Product product = em.find(Product.class, PRODUCT_ID);

                assertEquals(EXPECTED_SIZE, product.getGuides().size());

                Set<Guide> guides = product.getGuides();
                Iterator<Guide> iterator = guides.iterator();

                assertEquals(PRODUCT_ID, iterator.next().getParent().getId().longValue());

                tx.commit();
                em.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            TM.rollback();
        }
    }
}
