package hibernate.tricks.mapping.collections;

import hibernate.tricks.env.AbstractHibernateTest;
import hibernate.tricks.model.mapping.collections.map.Language;
import hibernate.tricks.model.mapping.collections.map.Product;
import org.testng.annotations.Test;

import javax.persistence.EntityManager;
import javax.transaction.UserTransaction;
import java.util.Map;

import static org.testng.AssertJUnit.assertEquals;

public class MapTest extends AbstractHibernateTest {

    private static final String FILE_NAME_1 = "FILE_NAME_1";
    private static final String FILE_NAME_2 = "FILE_NAME_2";
    private static final String FILE_NAME_3 = "FILE_NAME_3";

    private static final int EXPECTED_SIZE = 3;

    @Override
    public void configurePersistenceUnit() throws Exception {
        configurePersistenceUnit("MapPU");
    }

    @Test
    public void testMap() {
        UserTransaction tx = TM.getUserTransaction();
        long PRODUCT_ID;

        try {
            {
                tx.begin();

                EntityManager em = JPA.createEntityManager();

                Product product = new Product();
                em.persist(product);
                PRODUCT_ID = product.getId();

                product.getGuides().put(Language.RUSSIAN, FILE_NAME_1);
                product.getGuides().put(Language.ENGLISH, FILE_NAME_2);
                product.getGuides().put(Language.DEUTSCHE, FILE_NAME_3);

                tx.commit();
                em.close();
            }
            {
                tx.begin();

                EntityManager em = JPA.createEntityManager();

                Product product = em.find(Product.class, PRODUCT_ID);
                Map<Language, String> guides = product.getGuides();

                assertEquals(EXPECTED_SIZE, product.getGuides().size());
                assertEquals(FILE_NAME_1, guides.get(Language.RUSSIAN));
                assertEquals(FILE_NAME_2, guides.get(Language.ENGLISH));
                assertEquals(FILE_NAME_3, guides.get(Language.DEUTSCHE));

                tx.commit();
                em.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            TM.rollback();
        }
    }
}
