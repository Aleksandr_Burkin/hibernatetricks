package hibernate.tricks.mapping.collections;

import hibernate.tricks.env.AbstractHibernateTest;
import hibernate.tricks.model.mapping.collections.listnotunique.Product;
import org.testng.annotations.Test;

import javax.persistence.EntityManager;
import javax.transaction.UserTransaction;
import java.math.BigInteger;
import java.util.List;

import static org.testng.AssertJUnit.assertEquals;

public class ListNotUniqueTest extends AbstractHibernateTest {

    private static final String FILE_NAME_1 = "FILE_NAME_1";
    private static final String FILE_NAME_2 = "FILE_NAME_2";
    private static final String FILE_NAME_3 = "FILE_NAME_3";

    private static final int EXPECTED_SIZE = 4;

    private static final String SQL = "SELECT * FROM guide";

    @Override
    public void configurePersistenceUnit() throws Exception {
        configurePersistenceUnit("ListNotUniquePU");
    }

    @Test
    public void testListNotUnique() {
        UserTransaction tx = TM.getUserTransaction();
        long PRODUCT_ID;

        try {
            {
                tx.begin();

                EntityManager em = JPA.createEntityManager();

                Product product = new Product();
                em.persist(product);
                PRODUCT_ID = product.getId();

                product.getGuides().add(FILE_NAME_1);
                product.getGuides().add(FILE_NAME_2);
                product.getGuides().add(FILE_NAME_3);
                product.getGuides().add(FILE_NAME_3);

                tx.commit();
                em.close();
            }
            {
                tx.begin();

                EntityManager em = JPA.createEntityManager();

                Product product = em.find(Product.class, PRODUCT_ID);

                assertEquals(EXPECTED_SIZE, product.getGuides().size());
                assertEquals(FILE_NAME_1, product.getGuides().iterator().next());

                List<Object[]> list = em.createNativeQuery(SQL).getResultList();

                assertEquals(PRODUCT_ID, ((BigInteger)list.get(0)[0]).longValue());
                assertEquals(PRODUCT_ID, ((BigInteger)list.get(1)[0]).longValue());
                assertEquals(PRODUCT_ID, ((BigInteger)list.get(2)[0]).longValue());
                assertEquals(PRODUCT_ID, ((BigInteger)list.get(3)[0]).longValue());

                assertEquals(FILE_NAME_1, list.get(0)[1]);
                assertEquals(FILE_NAME_2, list.get(1)[1]);
                assertEquals(FILE_NAME_3, list.get(2)[1]);
                assertEquals(FILE_NAME_3, list.get(3)[1]);

                //Test ID
                assertEquals(1000, ((BigInteger)list.get(0)[2]).intValue());
                assertEquals(1001, ((BigInteger)list.get(1)[2]).intValue());
                assertEquals(1002, ((BigInteger)list.get(2)[2]).intValue());
                assertEquals(1003, ((BigInteger)list.get(3)[2]).intValue());

                tx.commit();
                em.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            TM.rollback();
        }
    }
}
