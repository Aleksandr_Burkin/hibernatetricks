package hibernate.tricks.mapping.collections;

import hibernate.tricks.env.AbstractHibernateTest;
import hibernate.tricks.model.mapping.collections.setembeddedid.Product;
import org.testng.annotations.Test;

import javax.persistence.EntityManager;
import javax.transaction.UserTransaction;
import java.math.BigInteger;
import java.util.List;

import static org.testng.AssertJUnit.assertEquals;
import static org.testng.AssertJUnit.assertFalse;

public class SetEmbeddedIdTest extends AbstractHibernateTest {

    private static final String FILE_NAME_1 = "FILE_NAME_1";
    private static final String FILE_NAME_2 = "FILE_NAME_2";
    private static final String FILE_NAME_3 = "FILE_NAME_3";

    private static final int EXPECTED_SIZE = 3;

    private static final String SQL = "SELECT * FROM guide";

    @Override
    public void configurePersistenceUnit() throws Exception {
        configurePersistenceUnit("SetEmbeddedIdPU");
    }

    @Test
    public void testSetEmbeddedId() {
        UserTransaction tx = TM.getUserTransaction();
        long PRODUCT_ID;

        try {
            {
                tx.begin();

                EntityManager em = JPA.createEntityManager();

                Product product = new Product();
                em.persist(product);
                PRODUCT_ID = product.getId();

                product.getGuides().add(FILE_NAME_1);
                product.getGuides().add(FILE_NAME_2);
                product.getGuides().add(FILE_NAME_3);
                product.getGuides().add(FILE_NAME_3);

                tx.commit();
                em.close();
            }
            {
                tx.begin();

                EntityManager em = JPA.createEntityManager();

                Product product = em.find(Product.class, PRODUCT_ID);

                assertEquals(EXPECTED_SIZE, product.getGuides().size());

                List<Object[]> list = em.createNativeQuery(SQL).getResultList();

                assertEquals(PRODUCT_ID, ((BigInteger)list.get(0)[0]).longValue());
                assertEquals(PRODUCT_ID, ((BigInteger)list.get(1)[0]).longValue());
                assertEquals(PRODUCT_ID, ((BigInteger)list.get(2)[0]).longValue());

                assertFalse(list.get(0)[1].toString().isEmpty());
                assertFalse(list.get(1)[1].toString().isEmpty());
                assertFalse(list.get(2)[1].toString().isEmpty());

                tx.commit();
                em.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            TM.rollback();
        }
    }
}
