package hibernate.tricks.mapping.collections;

import hibernate.tricks.env.AbstractHibernateTest;
import hibernate.tricks.model.mapping.collections.linkedorderby.Product;
import org.testng.annotations.Test;

import javax.persistence.EntityManager;
import javax.transaction.UserTransaction;
import java.math.BigInteger;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import static org.testng.AssertJUnit.assertEquals;

public class LinkedOrderByTest extends AbstractHibernateTest {

    private static final String AUTHOR_1 = "CCC";
    private static final String AUTHOR_2 = "AAA";
    private static final String AUTHOR_3 = "BBB";

    private static final String FILE_NAME_1 = "FILE_NAME_1";
    private static final String FILE_NAME_2 = "FILE_NAME_2";
    private static final String FILE_NAME_3 = "FILE_NAME_3";

    private static final int EXPECTED_SIZE = 3;

    private static final String SQL = "SELECT * FROM guide";

    @Override
    public void configurePersistenceUnit() throws Exception {
        configurePersistenceUnit("LinkedOrderByPU");
    }

    @Test
    public void testLinkedOrderBy() {
        UserTransaction tx = TM.getUserTransaction();
        long PRODUCT_ID;

        try {
            {
                tx.begin();

                EntityManager em = JPA.createEntityManager();

                Product product = new Product();
                em.persist(product);
                PRODUCT_ID = product.getId();

                product.getGuides().put(AUTHOR_1, FILE_NAME_1);
                product.getGuides().put(AUTHOR_2, FILE_NAME_2);
                product.getGuides().put(AUTHOR_3, FILE_NAME_3);

                tx.commit();
                em.close();
            }
            {
                tx.begin();

                EntityManager em = JPA.createEntityManager();

                Product product = em.find(Product.class, PRODUCT_ID);

                assertEquals(EXPECTED_SIZE, product.getGuides().size());

                Map<String, String> guides = product.getGuides();
                Iterator<String> iterator = guides.values().iterator();

                //Sort check
                assertEquals(FILE_NAME_1, iterator.next());
                assertEquals(FILE_NAME_3, iterator.next());
                assertEquals(FILE_NAME_2, iterator.next());

                List<Object[]> list = em.createNativeQuery(SQL).getResultList();

                assertEquals(PRODUCT_ID, ((BigInteger)list.get(0)[0]).longValue());
                assertEquals(PRODUCT_ID, ((BigInteger)list.get(1)[0]).longValue());
                assertEquals(PRODUCT_ID, ((BigInteger)list.get(2)[0]).longValue());

                assertEquals(FILE_NAME_1, list.get(0)[1]);
                assertEquals(FILE_NAME_2, list.get(1)[1]);
                assertEquals(FILE_NAME_3, list.get(2)[1]);

                assertEquals(AUTHOR_1, (String)list.get(0)[2]);
                assertEquals(AUTHOR_2, (String)list.get(1)[2]);
                assertEquals(AUTHOR_3, (String)list.get(2)[2]);

                tx.commit();
                em.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            TM.rollback();
        }
    }
}
