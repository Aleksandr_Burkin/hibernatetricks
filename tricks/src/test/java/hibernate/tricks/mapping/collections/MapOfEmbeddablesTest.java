package hibernate.tricks.mapping.collections;

import hibernate.tricks.env.AbstractHibernateTest;
import hibernate.tricks.model.mapping.collections.mapofembeddables.Author;
import hibernate.tricks.model.mapping.collections.mapofembeddables.Guide;
import hibernate.tricks.model.mapping.collections.mapofembeddables.Product;
import org.testng.annotations.Test;

import javax.persistence.EntityManager;
import javax.transaction.UserTransaction;

import java.util.Map;

import static org.testng.AssertJUnit.assertEquals;

public class MapOfEmbeddablesTest extends AbstractHibernateTest {

    private static final String AUTHOR_1 = "AUTHOR_1";
    private static final String AUTHOR_2 = "AUTHOR_2";
    private static final String AUTHOR_3 = "AUTHOR_3";

    private static final Integer AGE_1 = 10;
    private static final Integer AGE_2 = 20;
    private static final Integer AGE_3 = 30;

    private static final String TITLE_1 = "TITLE_1";
    private static final String TITLE_2 = "TITLE_2";
    private static final String TITLE_3 = "TITLE_3";

    private static final Integer SIZE_1 = 100;
    private static final Integer SIZE_2 = 200;
    private static final Integer SIZE_3 = 300;

    private static final int EXPECTED_SIZE = 3;
    private static final int EXPECTED_REMOVED_SIZE = 2;

    @Override
    public void configurePersistenceUnit() throws Exception {
        configurePersistenceUnit("MapOfEmbeddablesPU");
    }

    @Test
    public void storeLoadCollection() throws Exception {
        UserTransaction tx = TM.getUserTransaction();

        try {
            tx.begin();

            EntityManager em = JPA.createEntityManager();

            Product product = new Product();

            Author author_1 = new Author(AUTHOR_1, AGE_1);
            product.getGuides().put(author_1, new Guide(TITLE_1, SIZE_1));
            Author author_2 = new Author(AUTHOR_2, AGE_2);
            product.getGuides().put(author_2, new Guide(TITLE_2, SIZE_2));
            Author author_3 = new Author(AUTHOR_3, AGE_3);
            product.getGuides().put(author_3, new Guide(TITLE_3, SIZE_3));
            Author author_4 = new Author(AUTHOR_3, AGE_3);
            product.getGuides().put(author_4, new Guide(TITLE_3, SIZE_3)); // Duplicate

            em.persist(product);
            tx.commit();
            em.close();

            Long PRODUCT_ID = product.getId();

            tx.begin();

            em = JPA.createEntityManager();

            product = em.find(Product.class, PRODUCT_ID);
            Map<Author, Guide> guides = product.getGuides();

            assertEquals(EXPECTED_SIZE, guides.size());
            assertEquals(TITLE_1, guides.get(new Author(AUTHOR_1, AGE_1)).getTitle());
            assertEquals(TITLE_2, guides.get(new Author(AUTHOR_2, AGE_2)).getTitle());
            assertEquals(TITLE_3, guides.get(new Author(AUTHOR_3, AGE_3)).getTitle());

            product.getGuides().remove(new Author(AUTHOR_1, AGE_1));

            tx.commit();
            em.close();

            tx.begin();

            em = JPA.createEntityManager();

            product = em.find(Product.class, PRODUCT_ID);

            assertEquals(product.getGuides().size(), EXPECTED_REMOVED_SIZE);

            tx.commit();
            em.close();
        } finally {
            TM.rollback();
        }
    }
}
