package hibernate.tricks.mapping.collections;

import hibernate.tricks.env.AbstractHibernateTest;
import hibernate.tricks.model.mapping.collections.sortnatural.Product;
import org.testng.annotations.Test;

import javax.persistence.EntityManager;
import javax.transaction.UserTransaction;
import java.util.Iterator;

import static org.testng.AssertJUnit.assertEquals;

public class SortNatural extends AbstractHibernateTest {

    private static final String AUTHOR_1 = "CCC";
    private static final String AUTHOR_2 = "AAA";
    private static final String AUTHOR_3 = "BBB";

    private static final String FILE_NAME_1 = "FILE_NAME_1";
    private static final String FILE_NAME_2 = "FILE_NAME_2";
    private static final String FILE_NAME_3 = "FILE_NAME_3";

    private static final int EXPECTED_SIZE = 3;

    @Override
    public void configurePersistenceUnit() throws Exception {
        configurePersistenceUnit("SortNaturalPU");
    }

    @Test
    public void testNatural() {
        UserTransaction tx = TM.getUserTransaction();
        long PRODUCT_ID;

        try {
            {
                tx.begin();

                EntityManager em = JPA.createEntityManager();

                Product product = new Product();
                em.persist(product);
                PRODUCT_ID = product.getId();

                product.getGuides().put(AUTHOR_1, FILE_NAME_1);
                product.getGuides().put(AUTHOR_2, FILE_NAME_2);
                product.getGuides().put(AUTHOR_3, FILE_NAME_3);

                tx.commit();
                em.close();
            }
            {
                tx.begin();

                EntityManager em = JPA.createEntityManager();

                Product product = em.find(Product.class, PRODUCT_ID);

                assertEquals(EXPECTED_SIZE, product.getGuides().size());

                Iterator<String> iterator = product.getGuides().values().iterator();

                assertEquals(FILE_NAME_2, iterator.next());
                assertEquals(FILE_NAME_3, iterator.next());
                assertEquals(FILE_NAME_1, iterator.next());

                tx.commit();
                em.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            TM.rollback();
        }
    }
}
