package hibernate.tricks.mapping.collections;

import hibernate.tricks.env.AbstractHibernateTest;
import hibernate.tricks.model.mapping.collections.order.Guide;
import hibernate.tricks.model.mapping.collections.order.GuideId;
import hibernate.tricks.model.mapping.collections.order.Product;
import org.testng.annotations.Test;

import javax.persistence.EntityManager;
import javax.transaction.UserTransaction;

import static org.testng.AssertJUnit.assertEquals;

public class OrderTest extends AbstractHibernateTest {

    private static final String FILE_NAME_1 = "FILE_NAME_1";
    private static final String FILE_NAME_2 = "FILE_NAME_2";
    private static final String FILE_NAME_3 = "FILE_NAME_3";

    @Override
    public void configurePersistenceUnit() throws Exception {
        configurePersistenceUnit("OrderPU");
    }

    @Test
    public void testOrder() {
        UserTransaction tx = TM.getUserTransaction();
        try {
            {
                tx.begin();

                EntityManager em = JPA.createEntityManager();

                Product product = new Product();
                em.persist(product);

                product.getGuides().add(FILE_NAME_1);
                product.getGuides().add(FILE_NAME_2);
                product.getGuides().add(FILE_NAME_3);

                tx.commit();
                em.close();
            }
            {
                tx.begin();

                EntityManager em = JPA.createEntityManager();

                Product product = em.find(Product.class, 1L);

                assertEquals(3, product.getGuides().size());

                Guide guide_1 = em.find(Guide.class, new GuideId(product.getId(), FILE_NAME_1));
                Guide guide_2 = em.find(Guide.class, new GuideId(product.getId(), FILE_NAME_2));
                Guide guide_3 = em.find(Guide.class, new GuideId(product.getId(), FILE_NAME_3));
                //TODO присваиваем GUIDE_ORDER больше чем фактическое количество гайдов
                guide_1.setGuideOrder(300L);
                guide_2.setGuideOrder(200L);
                guide_3.setGuideOrder(100L);

                tx.commit();
                em.close();
            }
            {
                tx.begin();

                EntityManager em = JPA.createEntityManager();

                Product product = em.find(Product.class, 1L);

                //TODO было добавлено еще около 300 элементов null !!!!!!!!!!
                assertEquals(301, product.getGuides().size());

                Object[] guides = product.getGuides().toArray();

                assertEquals(FILE_NAME_3, guides[100]);
                assertEquals(FILE_NAME_2, guides[200]);
                assertEquals(FILE_NAME_1, guides[300]);

                tx.commit();
                em.close();
            }
            {
                tx.begin();

                EntityManager em = JPA.createEntityManager();

                Product product = em.find(Product.class, 1L);
                Object[] guides = product.getGuides().toArray();
                product.getGuides().remove(guides[100]);

                //TODO после удаления первого гайда выполняются лишние insert-ы для оставшихся гайдов

                tx.commit();
                em.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            TM.rollback();
        }
    }
}
