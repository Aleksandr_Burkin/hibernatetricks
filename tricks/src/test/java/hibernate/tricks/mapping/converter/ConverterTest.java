package hibernate.tricks.mapping.converter;

import hibernate.tricks.env.AbstractHibernateTest;
import hibernate.tricks.model.mapping.converter.Amount;
import hibernate.tricks.model.mapping.converter.ProductConverter;
import org.testng.annotations.Test;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.transaction.UserTransaction;
import java.math.BigDecimal;
import java.util.Currency;
import java.util.Locale;

import static org.testng.AssertJUnit.assertEquals;

public class ConverterTest extends AbstractHibernateTest {

    private static final String EXPECTED_PRICE = "Amount{value=123.40, currency=CNY}";

    private static final String SQL = "select price from ProductConverter";

    @Override
    public void configurePersistenceUnit() throws Exception {
        configurePersistenceUnit("ConverterPU");
    }

    @Test
    public void testConverter() {
        try {
            {
                UserTransaction tx = TM.getUserTransaction();
                tx.begin();

                EntityManager em = JPA.createEntityManager();

                ProductConverter product = new ProductConverter();
                Amount price = new Amount(new BigDecimal(123.4), Currency.getInstance(Locale.CHINA));
                product.setPrice(price);

                em.persist(product);

                tx.commit();
                em.close();
            }

            {
                UserTransaction tx = TM.getUserTransaction();
                tx.begin();

                EntityManager em = JPA.createEntityManager();

                Query nativeQuery = em.createNativeQuery(SQL);
                String value = (String) nativeQuery.getResultList().get(0);

                assertEquals(EXPECTED_PRICE, value);

                tx.commit();
                em.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            TM.rollback();
        }
    }
}
